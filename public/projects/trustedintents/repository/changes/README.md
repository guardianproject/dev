<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>/README.md - Changes - Trusted Intents - Guardian Project Dev (ARCHIVED SITE)</title>
<meta name="description" content="Redmine" />
<meta name="keywords" content="issue,bug,tracker" />
<meta content="authenticity_token" name="csrf-param" />
<meta content="TFkt2Jobv7aDVO+oYsmsHbD4ygyDFiolu7KeLK56zU8=" name="csrf-token" />
<link rel='shortcut icon' href='../../../../favicon.ico%3F1371683577' />
<link href="../../../../stylesheets/jquery/jquery-ui-1.9.2.css%3F1371683503.css" media="all" rel="stylesheet" type="text/css" />
<link href="../../../../themes/a1/stylesheets/application.css%3F1386177785.css" media="all" rel="stylesheet" type="text/css" />

<script src="../../../../javascripts/jquery-1.8.3-ui-1.9.2-ujs-2.0.3.js%3F1371683503" type="text/javascript"></script>
<script src="../../../../javascripts/application.js%3F1371683577" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
$(window).load(function(){ warnLeavingUnsaved('The current page contains unsaved text that will be lost if you leave this page.'); });
//]]>
</script>


<!-- page specific tags -->
  <script src="../../../../javascripts/repository_navigation.js%3F1371683503" type="text/javascript"></script>
<link href="../../../../stylesheets/scm.css%3F1371683577.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body class="theme-A1 controller-repositories action-changes">
<div id="wrapper">
<div id="wrapper2">
<div id="wrapper3">
<div id="top-menu">
    <div id="account">
        <ul><li><a href="../../../../login.html" class="login">Sign in</a></li>
<li><a href="../../../../account/register.html" class="register">Register</a></li></ul>    </div>
    
    <ul><li><a href="../../../../index.html" class="home">Home</a></li>
<li><a href="../../../../projects.html" class="projects">Projects</a></li>
<li><a href="http://www.redmine.org/guide" class="help">Help</a></li></ul></div>

<div id="header">
    <div id="quick-search">
        <form accept-charset="UTF-8" action="../../search.html" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
        <input name="changesets" type="hidden" value="1" />
        <label for='q'>
          <a href="../../search.html" accesskey="4">Search</a>:
        </label>
        <input accesskey="f" class="small" id="q" name="q" size="20" type="text" />
</form>        
    </div>

    <h1><a href="../../../libs%3Fjump=repository.html" class="root">Developer Libraries</a> » Trusted Intents</h1>

    <div id="main-menu">
        <ul><li><a href="../../../trustedintents.html" class="overview">Overview</a></li>
<li><a href="../../activity.html" class="activity">Activity</a></li>
<li><a href="../../../trustedintents%3Fjump=issues.html" class="issues">Issues</a></li>
<li><a href="../../issues/new.html" accesskey="7" class="new-issue">New issue</a></li>
<li><a href="../../issues/calendar.html" class="calendar">Calendar</a></li>
<li><a href="../../news.html" class="news">News</a></li>
<li><a href="../../wiki.html" class="wiki">Wiki</a></li>
<li><a href="../../repository.html" class="repository selected">Repository</a></li></ul>
    </div>
</div>

<div id="main" class="nosidebar">
    <div id="sidebar">
        
        
    </div>

    <div id="content">
        
        

<div class="contextual">
  
<a href="../statistics.html" class="icon icon-stats">Statistics</a>

<form accept-charset="UTF-8" action="https://dev.guardianproject.info/projects/trustedintents/repository/changes/README.md" id="revision_selector" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>  <!-- Branches Dropdown -->
    | Branch: 
    <select id="branch" name="branch"><option value=""></option>
<option value="master" selected="selected">master</option></select>

    | Tag: 
    <select id="tag" name="tag"><option value=""></option>
<option value="0.0">0.0</option>
<option value="0.1">0.1</option>
<option value="0.2">0.2</option></select>

    | Revision: 
    <input id="rev" name="rev" size="8" type="text" value="master" />
</form>
</div>

<h2><a href="../revisions/master/show.html">trustedintents</a>
    / <a href="README.md%3Frev=master.html">README.md</a>
@ master

</h2>


<p>
History |
    <a href="../revisions/master/entry/README.md.html">View</a> |
    <a href="../revisions/master/annotate/README.md.html">Annotate</a> |
<a href="../revisions/master/raw/README.md">Download</a>
(893 Bytes)
</p>






<form accept-charset="UTF-8" action="https://dev.guardianproject.info/projects/trustedintents/repository/diff/README.md" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
<table class="list changesets">
<thead><tr>
<th>#</th>
<th></th>
<th></th>
<th>Date</th>
<th>Author</th>
<th>Comment</th>
</tr></thead>
<tbody>
<tr class="changeset odd">
<td class="id">
  <a href="../revisions/42b6cdc1b32fa12f6c9af6ba022917433f7e4637.html" title="Revision 42b6cdc1">42b6cdc1</a>
</td><td class="checkbox"><input checked="checked" id="cb-1" name="rev" onclick="$(&#x27;#cbto-2&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="42b6cdc1b32fa12f6c9af6ba022917433f7e4637" /></td>
<td class="checkbox"></td>
<td class="committed_on">08/28/2015 12:24 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>document how LGPL works with Java</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="../revisions/9573c767ad17551e62cbc88acf5fdc08e4101855.html" title="Revision 9573c767">9573c767</a>
</td><td class="checkbox"><input id="cb-2" name="rev" onclick="$(&#x27;#cbto-3&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="9573c767ad17551e62cbc88acf5fdc08e4101855" /></td>
<td class="checkbox"><input checked="checked" id="cbto-2" name="rev_to" onclick="if ($(&#x27;#cb-2&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-1&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="9573c767ad17551e62cbc88acf5fdc08e4101855" /></td>
<td class="committed_on">06/24/2014 08:40 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>link to trusted intents blog post in README</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="../revisions/411880e4f70f494d80762d12e6301d2044ea372b.html" title="Revision 411880e4">411880e4</a>
</td><td class="checkbox"></td>
<td class="checkbox"><input id="cbto-3" name="rev_to" onclick="if ($(&#x27;#cb-3&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-2&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="411880e4f70f494d80762d12e6301d2044ea372b" /></td>
<td class="committed_on">06/23/2014 11:21 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>add README and LICENSE</p></td>
</tr>
</tbody>
</table>
<input type="submit" value="View differences" />
</form>



        
        <div style="clear:both;"></div>
    </div>
</div>
</div>

<div id="ajax-indicator" style="display:none;"><span>Loading...</span></div>
<div id="ajax-modal" style="display:none;"></div>

<div id="footer">
  <div class="bgl"><div class="bgr">
    Powered by <a href="http://www.redmine.org/">Redmine</a> &copy; 2006-2013 Jean-Philippe Lang
  </div></div>
</div>
</div>
</div>

</body>
</html>

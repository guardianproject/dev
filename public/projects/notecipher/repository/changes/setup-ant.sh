<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>/setup-ant.sh - Changes - NoteCipher - Guardian Project Dev (ARCHIVED SITE)</title>
<meta name="description" content="Redmine" />
<meta name="keywords" content="issue,bug,tracker" />
<meta content="authenticity_token" name="csrf-param" />
<meta content="TFkt2Jobv7aDVO+oYsmsHbD4ygyDFiolu7KeLK56zU8=" name="csrf-token" />
<link rel='shortcut icon' href='/favicon.ico?1371683577' />
<link href="/stylesheets/jquery/jquery-ui-1.9.2.css?1371683503" media="all" rel="stylesheet" type="text/css" />
<link href="/themes/a1/stylesheets/application.css?1386177785" media="all" rel="stylesheet" type="text/css" />

<script src="/javascripts/jquery-1.8.3-ui-1.9.2-ujs-2.0.3.js?1371683503" type="text/javascript"></script>
<script src="/javascripts/application.js?1371683577" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
$(window).load(function(){ warnLeavingUnsaved('The current page contains unsaved text that will be lost if you leave this page.'); });
//]]>
</script>


<!-- page specific tags -->
  <script src="/javascripts/repository_navigation.js?1371683503" type="text/javascript"></script>
<link href="/stylesheets/scm.css?1371683577" media="screen" rel="stylesheet" type="text/css" />
</head>
<body class="theme-A1 controller-repositories action-changes">
<div id="wrapper">
<div id="wrapper2">
<div id="wrapper3">
<div id="top-menu">
    <div id="account">
        <ul><li><a href="/login" class="login">Sign in</a></li>
<li><a href="/account/register" class="register">Register</a></li></ul>    </div>
    
    <ul><li><a href="/" class="home">Home</a></li>
<li><a href="/projects" class="projects">Projects</a></li>
<li><a href="http://www.redmine.org/guide" class="help">Help</a></li></ul></div>

<div id="header">
    <div id="quick-search">
        <form accept-charset="UTF-8" action="/projects/notecipher/search" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
        <input name="changesets" type="hidden" value="1" />
        <label for='q'>
          <a href="/projects/notecipher/search" accesskey="4">Search</a>:
        </label>
        <input accesskey="f" class="small" id="q" name="q" size="20" type="text" />
</form>        
    </div>

    <h1><a href="/projects/core?jump=repository" class="root">Core Apps</a> » NoteCipher</h1>

    <div id="main-menu">
        <ul><li><a href="/projects/notecipher" class="overview">Overview</a></li>
<li><a href="/projects/notecipher/activity" class="activity">Activity</a></li>
<li><a href="/projects/notecipher/issues" class="issues">Issues</a></li>
<li><a href="/projects/notecipher/issues/calendar" class="calendar">Calendar</a></li>
<li><a href="/projects/notecipher/wiki" class="wiki">Wiki</a></li>
<li><a href="/projects/notecipher/repository" class="repository selected">Repository</a></li></ul>
    </div>
</div>

<div id="main" class="nosidebar">
    <div id="sidebar">
        
        
    </div>

    <div id="content">
        
        

<div class="contextual">
  
<a href="/projects/notecipher/repository/statistics" class="icon icon-stats">Statistics</a>

<form accept-charset="UTF-8" action="/projects/notecipher/repository/changes/setup-ant.sh" id="revision_selector" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>  <!-- Branches Dropdown -->
    | Branch: 
    <select id="branch" name="branch"><option value=""></option>
<option value="master" selected="selected">master</option></select>

    | Tag: 
    <select id="tag" name="tag"><option value=""></option>
<option value="0.0.2">0.0.2</option>
<option value="0.0.4.1">0.0.4.1</option>
<option value="0.0.7">0.0.7</option></select>

    | Revision: 
    <input id="rev" name="rev" size="8" type="text" value="master" />
</form>
</div>

<h2><a href="/projects/notecipher/repository/revisions/master/show">notecipher</a>
    / <a href="/projects/notecipher/repository/changes/setup-ant.sh?rev=master">setup-ant.sh</a>
@ master

</h2>


<p>
History |
    <a href="/projects/notecipher/repository/revisions/master/entry/setup-ant.sh">View</a> |
    <a href="/projects/notecipher/repository/revisions/master/annotate/setup-ant.sh">Annotate</a> |
<a href="/projects/notecipher/repository/revisions/master/raw/setup-ant.sh">Download</a>
(432 Bytes)
</p>






<form accept-charset="UTF-8" action="/projects/notecipher/repository/diff/setup-ant.sh" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
<table class="list changesets">
<thead><tr>
<th>#</th>
<th></th>
<th></th>
<th>Date</th>
<th>Author</th>
<th>Comment</th>
</tr></thead>
<tbody>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/notecipher/repository/revisions/9443ce523182dcabe290740af52a3c6674826719" title="Revision 9443ce52">9443ce52</a>
</td><td class="checkbox"><input checked="checked" id="cb-1" name="rev" onclick="$(&#x27;#cbto-2&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="9443ce523182dcabe290740af52a3c6674826719" /></td>
<td class="checkbox"></td>
<td class="committed_on">04/07/2014 10:54 am</td>
<td class="author">Abel Luck </td>
<td class="comments"><p>update setup-ant.sh</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/notecipher/repository/revisions/9d41cb9d312bf454ed031116578eda1847eea2b9" title="Revision 9d41cb9d">9d41cb9d</a>
</td><td class="checkbox"><input id="cb-2" name="rev" onclick="$(&#x27;#cbto-3&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="9d41cb9d312bf454ed031116578eda1847eea2b9" /></td>
<td class="checkbox"><input checked="checked" id="cbto-2" name="rev_to" onclick="if ($(&#x27;#cb-2&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-1&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="9d41cb9d312bf454ed031116578eda1847eea2b9" /></td>
<td class="committed_on">03/10/2014 07:23 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>remove any duplicate versions of sqlcipher.jar (i.e. in cacheword)</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/notecipher/repository/revisions/30e0978cb77610064bea4b06c9ffcd5e9038b061" title="Revision 30e0978c">30e0978c</a>
</td><td class="checkbox"><input id="cb-3" name="rev" onclick="$(&#x27;#cbto-4&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="30e0978cb77610064bea4b06c9ffcd5e9038b061" /></td>
<td class="checkbox"><input id="cbto-3" name="rev_to" onclick="if ($(&#x27;#cb-3&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-2&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="30e0978cb77610064bea4b06c9ffcd5e9038b061" /></td>
<td class="committed_on">11/11/2013 04:53 pm</td>
<td class="author">Abel Luck </td>
<td class="comments"><p>make setup-ant copy the support library when necessary</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/notecipher/repository/revisions/7ea28e73e72a331f73fa59fd90d1db1e925e68c3" title="Revision 7ea28e73">7ea28e73</a>
</td><td class="checkbox"><input id="cb-4" name="rev" onclick="$(&#x27;#cbto-5&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="7ea28e73e72a331f73fa59fd90d1db1e925e68c3" /></td>
<td class="checkbox"><input id="cbto-4" name="rev_to" onclick="if ($(&#x27;#cb-4&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-3&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="7ea28e73e72a331f73fa59fd90d1db1e925e68c3" /></td>
<td class="committed_on">07/30/2013 06:38 pm</td>
<td class="author">Abel Luck </td>
<td class="comments"><p>make setup-ant.sh executable</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/notecipher/repository/revisions/2bf44a9de6af1c03f1244b92ef37e4cea3f756fc" title="Revision 2bf44a9d">2bf44a9d</a>
</td><td class="checkbox"></td>
<td class="checkbox"><input id="cbto-5" name="rev_to" onclick="if ($(&#x27;#cb-5&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-4&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="2bf44a9de6af1c03f1244b92ef37e4cea3f756fc" /></td>
<td class="committed_on">07/30/2013 06:07 pm</td>
<td class="author">Abel Luck </td>
<td class="comments"><p>add setup-ant.sh script</p></td>
</tr>
</tbody>
</table>
<input type="submit" value="View differences" />
</form>



        
        <div style="clear:both;"></div>
    </div>
</div>
</div>

<div id="ajax-indicator" style="display:none;"><span>Loading...</span></div>
<div id="ajax-modal" style="display:none;"></div>

<div id="footer">
  <div class="bgl"><div class="bgr">
    Powered by <a href="http://www.redmine.org/">Redmine</a> &copy; 2006-2013 Jean-Philippe Lang
  </div></div>
</div>
</div>
</div>

</body>
</html>

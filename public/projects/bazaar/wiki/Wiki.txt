h1. Bazaar Wiki

Bazaar is our focused effort to make the "F-Droid":https://f-droid.org app store the most private and secure available, while embedding the best known methods for guaranteeing access no matter the conditions of the internet:

* share apps on your phone with people nearby using WiFi, Bluetooth, NFC, SDCard, etc
* audit installed apps by comparing them to the versions that other people have installed to make sure they are not malware
* use decentralized app stores from all sorts of organizations
* securely build and distribute app releases
* curate your own collections of media and apps


h2. Overview

* [[Concept Note]]
* [[User Stories]]
* [[Bazaar Phase 2 OTF Proposal]]
* [[Questions + Answers]]

h2. Activities and Research

* "research published on our blog":https://guardianproject.info/tag/bazaar
* [[FDroid Audit]]
* [[Auditing Existing APKs]]
* [[Bootstrapping Trust]]
* [[Local Data Transfer]]
* [[OTRDATA Integration Plan]]
* [[trustedintents:Wiki|Trusted Intent Interaction]]
* [[Chained TLS Cert Verification]]
* [[Signing the Local APK Index]]
* [[Improving the APK Signing Procedure]]
* [["Swap" apps]]
* [[Swap over bluetooth (in development)]]
* [[Ideas for the Next Phase]]

h2. Related Discussions

* posts on our blog: https://guardianproject.info/tag/bazaar/
* posts on the FDroid forum: https://f-droid.org/forums/tag/bazaar/
* [[Oct 23rd IRC Scrum log]]
* [[Nov 21st IRC log about identifying repos]]
* "F-Droid and decentralized trust convo on twitter":https://twitter.com/guardianproject/status/398092213651251201
* [[OpenITP UX Hackathon - Cydia/Community Notes]]
* [[March 26th IRC Scrum log]]


h2. Code Repositories

* "FDroid Android client":https://gitlab.com/fdroid/fdroidclient - the Android app store 
* "FDroid server tools":https://gitlab.com/fdroid/fdroidserver - the tools for managing app repos
* "androidobservatory":https://github.com/cpu/AndroidObservatory - website to present information about APKs


h2. Relevant F-Droid Issues

Whenever possible we should try to frame our work in terms of the F-Droid development process. If we can fix issues in F-Droid by submitting the functionality that we need for Bazaar, then its a win-win.

* "Resumeable downloads?":https://f-droid.org/repository/issues/?do=view_issue&issue=393 - p2p and tor will mean lots of flaky connections
* "Repo as virtual category in client":https://f-droid.org/repository/issues/?do=view_issue&issue=262 - we will need a way to represent what is on the device on the other side of a p2p sync
* "backgrounding apk download":https://f-droid.org/repository/issues/?do=view_issue&issue=307 - downloading via Tor and OTRDATA could be slow
* "Method for suggesting users uninstall an apk":https://f-droid.org/repository/issues/?do=view_issue&issue=144 - if an APK proves to be compromised, it should be able to be revoked and the client should recognize that


h2. Design Assets

*Starting Point*
Wifi-QR/IP screen: s3_wifi_QR.ai (attached)

*These documents cover the UI for all of the design we've discussed as of May 30th, 2014. I've labeled them as p01 for 'phase 1'.*
Illustrator file: swapUI_p01.ai
Images of each screen (22 total): swapUI_p01.zip
Diagram of the swap workflow and an outline of the other UI components: workflow_diag_p01.pdf

*Phase 2 Design
SVG files are attached below as 'swap_p2_v1.zip'
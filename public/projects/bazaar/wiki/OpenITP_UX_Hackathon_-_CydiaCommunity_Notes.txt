* Blogpost about Bazaar: https://guardianproject.info/2013/11/18/turn-your-device-into-an-app-store/
* Core concept: https://dev.guardianproject.info/projects/bazaar/wiki/Core_Concept
** This page should note that Cydia is a widely-used decentralized software installer for mobile (based on APT) that has tons of people running their own repositories to distribute their own software, including some experiments running on-device repositories: http://www.idownloadblog.com/2011/09/03/how-to-create-your-own-cydia-repo/ / http://www.redmondpie.com/host-your-very-own-repository-on-cydia-with-irepo-for-iphone-and-ipad-jailbreak-tweak/
* User stories: https://dev.guardianproject.info/projects/bazaar/wiki/User_Stories
** (semi unrelated to this team, Twine game system for prototyping interactions - http://www.auntiepixelante.com/twine/ )
* "bazaar" has some inconvenient name collisions

h3. if this were a blog post, what would the title be? what's the theme?

here are our guesses for why this should exist. here are some obstacles (that we can think of) to getting it exist.

h3. more notes about cydia stuff

so an idea for bazaar-like results on jailbroken ios: set up a package like "iRepo" (linked above) that includes a lighttpd configuration file and depends on lighttpd ( http://cydia.saurik.com/info/lighttpd/ ), ask people to turn on "personal hotspot" ( http://support.apple.com/kb/ts2756 ) to share over bluetooth, and then ask people to add the device as a repository. if they have a phone that doesn't already have personal hotspot (some carriers don't support it), they can install a package that enables personal hotspot, like tetherme (which is $5). also possibly vaguely interesting for prototyping: https://code.google.com/p/cydia-ios-lighttpd-php-mysql-web-stack/


Could be interesting to document how to do this and share the link in the jailbreaking community (such as by linking it on http://www.reddit.com/r/jailbreak/ ), to see if people are interested in it and have ideas for how to use it - cheap customer validation.

h3. fitting what people want

How do we encourage the use of Bazaar?

one friend is interested in easy developer-to-phone app distribution (along with easy friend-to-friend app distribution)

Who can we find to talk to who would actually want to use this, to find out what they would find useful? (informal customer validation)

Seems hard to find people to interview who have limited internet access, since they have limited internet access - argh. Or people who only have access to sketchy app stores, since they probably don't speak English - argh.

User stories: https://dev.guardianproject.info/projects/bazaar/wiki/User_Stories

Promote it as a safe alternative to sanctioned/blocked app stores (contextual) and the sketchy sites, which most users already suspect of being filled with malicious apps (or maybe they dont).  A few examples of malicious apps dowloanded from random webistes might promote users to use Bazaar. Connecting with trusted groups in the diaspora with community outreach to promote and vet Bazaar. Use cureent trust networks in the diaspora.  

* [There aren't all that many examples of traditionally malicious apps (information stealing, etc.) even in very popular sketchy pirate app stores though. But government intervention in communication apps is a plausible/worrying thing.]
** there are examples of backdoored circumvention tools distributed through alternative sources ( There has been a few Psiphon clients, both Windows and Android. And the most known one is: https://citizenlab.org/2012/05/iranian-anti-censorship-software-simurgh-circulated-with-malicious-backdoor-2/ -  although it was a windows only software, I think the example is still relavant.
*** [Nice example, thanks!!]

Bazaar can also be used to distribute apps that are no longer on the app store but are still installed on some user's devices. The FlappyBird use-case?

* Talking about this would get into copyright infringement stuff that isn't so good for reputation...probably. More legit-sounding to focus on communication tool distribution for revolutions
** Are there cases of apps being removed for reasons related to platform policy but not piracy? e.g. Google removing apps that compete with their core services? Something that wouldn't cause the same reputational concerns? Maybe someone knows of examples.... 
*** Oh yeah, Apple has removed or rejected lots of politically controversial apps, but there are already alternative distribution paths for them (websites that distribute pirated apps also distribute them) - http://www.wired.com/dangerroom/2012/08/drone-app/ http://bits.blogs.nytimes.com/2011/09/13/game-that-critiques-apple-vanishes-from-app-store/ etc. 
*** (( Adblock Plus is a good example of an app that Google censored that we would feel comfortable people Bazaar'ing to each other ))

h3. Encouraging the uptake of "run your own" F-Droid repositories:

Cydia has a distributed community with 1000+ individual repos. Repositories as a first-class feature of the cydia app. F-Droid repository support is largely vestigial, and as a result few individual repositories have been created.

A lot of the large independent Cydia repositories are language-specific; the default/core Cydia repositories are fairly English-specific, so jailbreakers who speak other languages self-organize and run their own repositories and forums.

h3. explanation for why we care about the below social/technical trust questions

Decentralized peer-to-peer app distribution introduces lots of room for people to modify and distribute apps with malware added. We want people to be able to check, with some reasonable amount of confidence, that they're using an app that hasn't been tampered with to add malware or evil government stuff. Bazaar requires users to allow "installation from third party sources", exposing them to threats not otherwise present if the user only installs applications from the Google Play store.

Assuming that some of the current apps are malicious (downloaded from random websites do to sanctions and censorship), this could enhance promotion and trust in Bazaar as an Alternative. 

h4. social forms of trust

What are the existing social structures + social media sources that people will be using alongside Bazaar? (How do we build on existing trust networks?).

How can we exploit the "trusted techie" angle. If one technically sophisticated individual is bootstrapping friends it is likely those friends defer to the technical user for questions of trust/safety.

Using trusted networks and organizations in the diaspora. Students and individuals travlleing back to home countries. 

What does the "information" page for an app look like? 
* Something along the lines of: https://androidobservatory.org/app/6270E8DD74F67A2C117335B5278AF91EDF440C28  ? What other information would be useful? How can it be made more "accessible"?

Examples of already established social structure and practices of peer to peer sharing. Especially offline methods that are more accessible and widely used to exchange other forms of information. 

Where does this information come from?
* Organzaiton in contact with targeted endusers. Localization, translation, community management adn outreach.

Will Bazaar run some kind of centralized web site to help provide information? (languages)

Maybe a guidline that can also be shared among users, emailed?

h4. technical forms of trust

How do users verify that an app is one they can trust? (What kind of signature checking/verifying is readable/usable for people?) How do we deal with people having different legitimate versions of the same app, causing different signatures?

How to solicit information about app signatures/sources?
*    When a user uploads an APK sample to the Android Observatory we want them to be able to include contextual information about where they found the app, suspicions they may have about the integrity/quality, etc. Current upload page offers no chance to provide this information ( https://androidobservatory.org/upload). Perhaps the most important question is "Where did you get this app sample? Your phone? An app store? A forum? A friend?"
 * Similarly, it would be advantageous to allow users/uploaders to "tag" signatures when they know more about the certificate than we do. I.e. there are a number of "public" private keys used by developers: https://github.com/CyanogenMod/android_build/tree/gingerbread/target/product/security
*    Example of an app signed with the CM platform key:
** https://androidobservatory.org/app/07D66B55D7BB7318003C38E40FF6DF2B8F68CEB1

* How to present errors/problems with apps?
** Types: of errors/problems we can detect:
***  Signature mismatches of Applications - two package names using a different set of signing certificates
*** Two apps claiming to be the same version code with different hashes - eg two "Version 5's" of Facebook should be byte for byte identical.

An issue from many other projects: signature mismatches are usually innocent technical errors with setup for repositories and packages, so users learn that these errors should be ignored, even if they have scary language. (On Android it isn't possible to install apps without a signature, but debug keys will often be used accidentally)

* E.g. of sig mismatches:
** SwiftKey 3:
*** https://androidobservatory.org/app/3A0BEA753B955560165CC961F7EF64C0A80EFA07
*** https://androidobservatory.org/app/1E53BD6A8B80C5F39E9ED9094578B27EE0FCB4D7
** TweetLanes:
*** https://androidobservatory.org/app/6270E8DD74F67A2C117335B5278AF91EDF440C28
*** https://androidobservatory.org/app/C10BB6A335F603E589F3DA15EB71AA170E690C46

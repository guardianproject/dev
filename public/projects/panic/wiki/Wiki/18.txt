h1. Wiki

h4. Overview

* [[PanicLib - Initial Concept]]
* [[Summary + Objectives]]
* [[Technical Foundation]]
* [[User Stories]]
* [[Metrics + Evaluation]]
* [[Timeline + Milestones]]

h4. Apps and other outputs

* LocationPrivacy app on our "FDroid repo":https://guardianproject.info/fdroid, "Google Play":https://play.google.com/store/apps/details?id=info.guardianproject.locationprivacy, and "GitHub":https://github.com/guardianproject/LocationPrivacy
* [[Panic Framework]]
* [[Trigger Ideas]]
* [[Action Ideas]]
* [[Design Patterns]]

h4. Related Projects

* [[Amnesty Panic Button]]

h4. Old Links (for the old app)

* Code https://github.com/guardianproject/InTheClear
* Translations - English, Farsi, Arabic, Chinese - https://www.transifex.com/projects/p/intheclear

h4. Design Progress

* panic_design_patterns_v1.pdf, October 15, 2015 
These design patterns outline the general user experience for connecting responder and trigger apps, testing, and using a trigger app. In these examples, a generic trigger app is used to demonstrate the patterns. Apps built by the Guardian Project that have panic settings are shown as example responders.
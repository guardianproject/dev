<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <title>Panic: News</title>
  <link rel="self" href="https://dev.guardianproject.info/projects/panic/news.atom"/>
  <link rel="alternate" href="https://dev.guardianproject.info/projects/panic/news"/>
  <id>https://dev.guardianproject.info/</id>
  <updated>2016-10-17T14:00:51Z</updated>
  <author>
    <name>Guardian Project Dev (ARCHIVED SITE)</name>
  </author>
  <generator uri="http://www.redmine.org/">
Redmine  </generator>
  <entry>
    <title>New trigger samples posted</title>
    <link rel="alternate" href="https://dev.guardianproject.info/news/276"/>
    <id>https://dev.guardianproject.info/news/276</id>
    <updated>2016-10-17T14:00:51Z</updated>
    <author>
      <name>n8fr8</name>
      <email>nathan@guardianproject.info</email>
    </author>
    <content type="html">
Read on here: &lt;a class="external" href="https://guardianproject.info/2016/10/17/if-this-then-panic-sample-code-for-triggering-emergency-alerts/"&gt;https://guardianproject.info/2016/10/17/if-this-then-panic-sample-code-for-triggering-emergency-alerts/&lt;/a&gt;&lt;br /&gt;and find the code here: &lt;a class="external" href="https://github.com/n8fr8/PanicKitSamples"&gt;https://github.com/n8fr8/PanicKitSamples&lt;/a&gt;
&lt;hr /&gt;


	&lt;p&gt;Earlier this year, we announced the PanicKit Library for Android and Ripple, our basic app for alerts any compatible app that you are in an emergency situation. Rather than build a solitary, enclosed “panic button” app that only can provide a specific set of functionality, we decided, as we often do, to build a framework, and encourage others to participate. Since then, we’ve had over 10 different apps implement PanicKit responder functionality, including Signal, OpenKeyChain, Umbrella app, StoryMaker and Zom.&lt;/p&gt;


	&lt;p&gt;It is great to have so many apps implement helpful features for users to react during an emergency situation. This might include sending an emergency message, putting sensitive data behind a password, hiding the app icon, or even wiping data. All of this can be triggered by a simple tap and swipe on the Ripple’s app user interface.&lt;/p&gt;


	&lt;p&gt;However, we would like to promote PanicKit trigger functionality that goes beyond something a user has to actively do, or at least obviously do. In many emergency scenarios, the user might be unable to actively trigger a panic, because they are unconscious, detained or have had their device taken away. In some cases, the activation may need to be subtle, such typing an incorrect phone number. In others, rapidly pressing a button or shaking the phone, may be safer and easier than unlocking your device and using an app.&lt;/p&gt;


	&lt;p&gt;a truly panic-inducing situation&lt;/p&gt;


	&lt;p&gt;PanicKit works by connecting trigger apps with receiver apps. Triggers are what create the alert that there is an emergency or panic situation. Responders receive the alert, and take an appropriate, user configured or default action.&lt;/p&gt;


	&lt;p&gt;The new PanicKitSamples project demonstrates new possible triggers that could be implemented in an app like Ripple, or any app that wishes to do so. In the “info.guardianproject.fakepanicbutton.triggers” package, you will find the following classes:&lt;/p&gt;


	&lt;p&gt;BaseTrigger: a base class that handles launching of the “panic intent” from a set of stored preferences to trigger the responders&lt;/p&gt;


	&lt;p&gt;public static void launchPanicIntent (Context context)
{&lt;br /&gt;    final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());&lt;/p&gt;


	&lt;pre&gt;&lt;code&gt;String email = prefs.getString("email",null);&lt;br /&gt;    String phone = prefs.getString("phone",null);&lt;br /&gt;    String subject = prefs.getString("subject","panic message");&lt;br /&gt;    String message = prefs.getString("message","i triggered a panic!");&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;launchIntent(context, email, phone, subject, message);&lt;br /&gt;}&lt;/code&gt;&lt;/pre&gt;


	&lt;p&gt;public static void launchIntent (Context context, String emailAddress, String phoneNumber, String subject, String message)
{&lt;br /&gt;    final PackageManager pm = context.getPackageManager();&lt;br /&gt;    final Set&amp;lt;String&amp;gt; receiverPackageNames = PanicTrigger.getResponderActivities(context);&lt;/p&gt;


	&lt;pre&gt;&lt;code&gt;Intent intent = new Intent(Panic.ACTION_TRIGGER);&lt;/code&gt;&lt;/pre&gt;


	&lt;p&gt;GeoTrigger: Using the awesome “LOST” open-source geofencing library, this trigger sends a panic if the device moves outside of a pre-defined area (in this sample, it is Times Square NYC)&lt;/p&gt;


	&lt;p&gt;private void setupGeoFence ()
{&lt;/p&gt;


	&lt;pre&gt;&lt;code&gt;//setup geofence for Times Square area&lt;br /&gt;    String requestId = "geof1-timesSquare";&lt;br /&gt;    double latitude = 40.758896;&lt;br /&gt;    double longitude = -73.985130;&lt;br /&gt;    float radius = 0.0001f;&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;Geofence geofence = new Geofence.Builder()&lt;br /&gt;            .setRequestId(requestId)&lt;br /&gt;            .setCircularRegion(latitude, longitude, radius)&lt;br /&gt;            .setExpirationDuration(Geofence.NEVER_EXPIRE)&lt;br /&gt;            .build();&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;GeofencingRequest request = new GeofencingRequest.Builder()&lt;br /&gt;            .addGeofence(geofence)&lt;br /&gt;            .build();&lt;/code&gt;&lt;/pre&gt;


	&lt;p&gt;MediaButtonTrigger: This trigger will notice multiple rapid pushes of a headset mic button or a bluetooth mic call button, and send a trigger.&lt;/p&gt;


	&lt;p&gt;public class MediaButtonTrigger extends BaseTrigger {&lt;/p&gt;


	&lt;pre&gt;&lt;code&gt;private static int mTriggerCount = 0;&lt;br /&gt;    private final static int TRIGGER_THRESHOLD = 3;&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;private static long mLastTriggerTime = -1;&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;public MediaButtonTrigger(Activity context)
{&lt;br /&gt;        super (context);&lt;br /&gt;    }&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;@Override&lt;br /&gt;    public void activateTrigger() {&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;//if a headset button or a bluetooth "call" button is pressed, trigger this&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;IntentFilter filter = new IntentFilter(Intent.ACTION_MEDIA_BUTTON);&lt;br /&gt;        MediaButtonIntentReceiver r = new MediaButtonIntentReceiver();&lt;br /&gt;        getContext().registerReceiver(r, filter);&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;}&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;public class MediaButtonIntentReceiver extends BroadcastReceiver {&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;public MediaButtonIntentReceiver() {&lt;br /&gt;            super();&lt;br /&gt;        }&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;@Override&lt;br /&gt;        public void onReceive(Context context, Intent intent) {&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;KeyEvent event = (KeyEvent)intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);&lt;br /&gt;            if (event == null) {&lt;br /&gt;                return;&lt;br /&gt;            }&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;int action = event.getAction();&lt;br /&gt;            if (action == KeyEvent.ACTION_DOWN) {&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;//check for 3 rapidly pressed key events&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;long triggerTime = new Date().getTime();&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;//if the trigger is the first one, or happened with a second of the last one, then count it&lt;br /&gt;                if (mLastTriggerTime == -1 || ((triggerTime - mLastTriggerTime)&amp;amp;lt;1000))&lt;br /&gt;                    mTriggerCount++;&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;mLastTriggerTime = triggerTime;&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;if (mTriggerCount &amp;gt; TRIGGER_THRESHOLD) {&lt;br /&gt;                    launchPanicIntent(context);&lt;br /&gt;                    mTriggerCount = 0;&lt;br /&gt;                }&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;}&lt;br /&gt;            abortBroadcast();&lt;br /&gt;        }&lt;br /&gt;    }&lt;br /&gt;}&lt;br /&gt;PhoneNumberTrigger (OutgoingCallReceiver): This trigger monitors phone calls, looking for a pre-defined fake “panic number”.&lt;/code&gt;&lt;/pre&gt;


	&lt;p&gt;public class OutgoingCallReceiver extends BroadcastReceiver {&lt;br /&gt;    @Override&lt;br /&gt;    public void onReceive(Context context, Intent intent) {&lt;/p&gt;
	&lt;pre&gt;&lt;code&gt;String phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;if (phoneNumber != null&lt;br /&gt;                &amp;#38;&amp;#38; phoneNumber.equals(PhoneNumberTrigger.PHONE_NUMBER_TRIGGER)) {&lt;br /&gt;            PhoneNumberTrigger.launchPanicIntent(context);&lt;br /&gt;        }&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;}&lt;br /&gt;}&lt;br /&gt;SuperShakeTrigger: This trigger looks for the phone being rapidly shaken. It could be expanded to wait for a series of shakes within a certain time window to avoid false positives.&lt;/code&gt;&lt;/pre&gt;


	&lt;p&gt;//setup shake detection using ShakeDetector library&lt;br /&gt;SensorManager sensorManager = (SensorManager) getContext().getSystemService(Context.SENSOR_SERVICE);&lt;/p&gt;


	&lt;p&gt;ShakeDetector sd = new ShakeDetector(new ShakeDetector.Listener()
{&lt;br /&gt;    public void hearShake() {&lt;/p&gt;
	&lt;pre&gt;&lt;code&gt;//you shook me!&lt;br /&gt;        launchPanicIntent(getContext());&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;}&lt;br /&gt;});&lt;/code&gt;&lt;/pre&gt;


	&lt;p&gt;sd.start(sensorManager);&lt;br /&gt;WifiTrigger: This triggers waits for the user to connect to a specific wifi network (in this sample “Starbucks”). It could also be set to trigger if the devices leaves the wifi network.&lt;/p&gt;


	&lt;p&gt;NetworkInfo netInfo = intent.getParcelableExtra (WifiManager.EXTRA_NETWORK_INFO);&lt;br /&gt;if (ConnectivityManager.TYPE_WIFI == netInfo.getType ()&lt;br /&gt;        &amp;#38;&amp;#38; netInfo.isConnected()) {&lt;/p&gt;


	&lt;pre&gt;&lt;code&gt;WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);&lt;br /&gt;    WifiInfo info = wifiManager.getConnectionInfo();&lt;br /&gt;    String ssid = info.getSSID();&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;//Check if I am connected to the "trigger" SSID, and if so send an alert!&lt;/code&gt;&lt;/pre&gt;


	&lt;pre&gt;&lt;code&gt;if (!TextUtils.isEmpty(ssid)&lt;br /&gt;        &amp;#38;&amp;#38; ssid.equals(WIFI_SSID_TRIGGER))
{&lt;br /&gt;        launchPanicIntent(getContext());&lt;br /&gt;    }&lt;br /&gt;}&lt;/code&gt;&lt;/pre&gt;


	&lt;p&gt;All of these samples are configured to work with the FakePanicButton sample app, which allows you to choose a contact to alert, and set a panic message. That said, these are meant to point in a direction of functionality, and have not been fully debugged or tested on all devices and OS versions.&lt;/p&gt;


	&lt;p&gt;If you have more ideas on other panic triggers that could be implemented, please share them here. We are also happy to take pull requests or fixes to our sample project, in order to improve on the ideas we have. Finally, we will announce more Panic responder and trigger apps, as they are available in the coming months. We looking forward to the continued growth of our PanicKit ecosystem, though of course, we hope even more for a world where there are less reasons to panic.&lt;/p&gt;    </content>
  </entry>
  <entry>
    <title>Signal adds Panic support</title>
    <link rel="alternate" href="https://dev.guardianproject.info/news/273"/>
    <id>https://dev.guardianproject.info/news/273</id>
    <updated>2016-09-01T08:18:56Z</updated>
    <author>
      <name>hans</name>
      <email>hans@guardianproject.info</email>
    </author>
    <content type="html">
&lt;p&gt;Signal for Android is now a panic receiver, as of the upcoming release v3.18.0:&lt;/p&gt;


	&lt;p&gt;&lt;a class="external" href="https://github.com/WhisperSystems/Signal-Android/pull/5550#issuecomment-243958565"&gt;https://github.com/WhisperSystems/Signal-Android/pull/5550#issuecomment-243958565&lt;/a&gt;&lt;/p&gt;    </content>
  </entry>
  <entry>
    <title>updated list of apps that support PanicKit</title>
    <link rel="alternate" href="https://dev.guardianproject.info/news/271"/>
    <id>https://dev.guardianproject.info/news/271</id>
    <updated>2016-07-27T10:35:50Z</updated>
    <author>
      <name>hans</name>
      <email>hans@guardianproject.info</email>
    </author>
    <content type="html">
&lt;p&gt;Here is the current known list of apps that support PanicKit:&lt;/p&gt;


	&lt;ul&gt;
	&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=info.guardianproject.ripple" class="external"&gt;Ripple&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=im.zom.messenger" class="external"&gt;Zom&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=info.guardianproject.browser" class="external"&gt;Orweb&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=org.secfirst.umbrella" class="external"&gt;Umbrella&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://f-droid.org/repository/browse/?fdid=org.schabi.newpipe" class="external"&gt;NewPipe&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=org.smssecure.smssecure" class="external"&gt;Silence aka SMSSecure&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=info.guardianproject.otr.app.im" class="external"&gt;ChatSecure&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=org.sufficientlysecure.keychain" class="external"&gt;OpenKeychain&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=acr.browser.barebones" class="external"&gt;Lightning Browser&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://briarproject.org/download.html" class="external"&gt;Briar Project&lt;/a&gt;&lt;/li&gt;
	&lt;/ul&gt;


	&lt;p&gt;Please post any new ones as comments here! Or email us at &lt;a class="email" href="mailto:support@guardianproject.info"&gt;support@guardianproject.info&lt;/a&gt;&lt;/p&gt;    </content>
  </entry>
  <entry>
    <title>state of panic pull requests</title>
    <link rel="alternate" href="https://dev.guardianproject.info/news/262"/>
    <id>https://dev.guardianproject.info/news/262</id>
    <updated>2016-03-14T16:13:12Z</updated>
    <author>
      <name>hans</name>
      <email>hans@guardianproject.info</email>
    </author>
    <content type="html">
&lt;p&gt;I just added a new pull request for Whisper Systems Signal:&lt;/p&gt;


	&lt;ul&gt;
	&lt;li&gt;&lt;a class="external" href="https://github.com/WhisperSystems/Signal-Android/pull/5341"&gt;https://github.com/WhisperSystems/Signal-Android/pull/5341&lt;/a&gt;&lt;/li&gt;
	&lt;/ul&gt;


	&lt;p&gt;Two more apps have merged support:&lt;/p&gt;


	&lt;ul&gt;
	&lt;li&gt;OpenKeychain &lt;a class="external" href="https://github.com/open-keychain/open-keychain/pull/1683"&gt;https://github.com/open-keychain/open-keychain/pull/1683&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;Lightning Browser &lt;a class="external" href="https://github.com/anthonycr/Lightning-Browser/pull/355"&gt;https://github.com/anthonycr/Lightning-Browser/pull/355&lt;/a&gt;&lt;/li&gt;
	&lt;/ul&gt;


	&lt;p&gt;And I updated the Panic Button request:&lt;/p&gt;


	&lt;ul&gt;
	&lt;li&gt;&lt;a class="external" href="https://github.com/PanicInitiative/PanicButton/pull/157"&gt;https://github.com/PanicInitiative/PanicButton/pull/157&lt;/a&gt;&lt;/li&gt;
	&lt;/ul&gt;    </content>
  </entry>
  <entry>
    <title>more apps supporting Panic</title>
    <link rel="alternate" href="https://dev.guardianproject.info/news/258"/>
    <id>https://dev.guardianproject.info/news/258</id>
    <updated>2016-01-19T11:50:26Z</updated>
    <author>
      <name>hans</name>
      <email>hans@guardianproject.info</email>
    </author>
    <content type="html">
&lt;p&gt;A number of new apps have recently added PanicKit support.  These are apps that now support PanicKit:&lt;/p&gt;


	&lt;ul&gt;
	&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=info.guardianproject.ripple" class="external"&gt;Ripple&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=im.zom.messenger" class="external"&gt;Zom&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=info.guardianproject.browser" class="external"&gt;Orweb&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=org.secfirst.umbrella" class="external"&gt;Umbrella&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://f-droid.org/repository/browse/?fdid=org.schabi.newpipe" class="external"&gt;NewPipe&lt;/a&gt;&lt;/li&gt;
	&lt;/ul&gt;


	&lt;p&gt;And these apps have merged PanicKit support, and will support it in their next release:&lt;/p&gt;


	&lt;ul&gt;
	&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=org.iilab.pb" class="external"&gt;PanicButton&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=org.smssecure.smssecure" class="external"&gt;SMSSecure&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=org.storymaker.app" class="external"&gt;StoryMaker&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=info.guardianproject.orfox" class="external"&gt;Orfox&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=info.guardianproject.courier" class="external"&gt;Courier&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://play.google.com/store/apps/details?id=info.guardianproject.otr.app.im" class="external"&gt;ChatSecure&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://briarproject.org/download.html" class="external"&gt;Briar&lt;/a&gt;&lt;/li&gt;
	&lt;/ul&gt;


	&lt;p&gt;These are open pull requests for making these apps panic responders.  If you want to see one of these apps gain PanicKit support, please post a message of support on the pull request!&lt;/p&gt;


	&lt;ul&gt;
	&lt;li&gt;&lt;a href="https://github.com/bpellin/keepassdroid/pull/72" class="external"&gt;KeePassDroid&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://github.com/anthonycr/Lightning-Browser/pull/355" class="external"&gt;Lightning Browser&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://github.com/open-keychain/open-keychain/pull/1683" class="external"&gt;OpenKeychain&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://github.com/schildbach/bitcoin-wallet/issues/288" class="external"&gt;Bitcoin Wallet&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a href="https://github.com/gustavomondron/twik/pull/16" class="external"&gt;Twik Password Manager&lt;/a&gt;&lt;/li&gt;
	&lt;/ul&gt;    </content>
  </entry>
  <entry>
    <title>new app release: Ripple</title>
    <link rel="alternate" href="https://dev.guardianproject.info/news/257"/>
    <id>https://dev.guardianproject.info/news/257</id>
    <updated>2015-12-30T14:43:23Z</updated>
    <author>
      <name>hans</name>
      <email>hans@guardianproject.info</email>
    </author>
    <content type="html">
&lt;p&gt;This is the first BETA version of our honed panic button user experience. We are working now to add support to as many other apps as possible. Orweb and Zom already have support, these apps are coming very soon: Orfox, Courier, StoryMaker, SMSSecure, Lightning Browser, Signal.&lt;/p&gt;


	&lt;p&gt;Get it on &lt;a href="https://play.google.com/store/apps/details?id=info.guardianproject.ripple" class="external"&gt;Google Play&lt;/a&gt; or our &lt;a href="https://guardianproject.info/fdroid" class="external"&gt;FDroid repo&lt;/a&gt;.&lt;/p&gt;


	&lt;p&gt;Ripple is a "panic button" that can send it's trigger message to any app that is a "panic responder". Such apps can do things like lock, disguise themselves, delete private data, send an emergency message, and more. It is meant for situations where there is time to react, but where users need to be sure it is not mistakenly set off. Here are two example scenarios:&lt;/p&gt;


	&lt;ul&gt;
	&lt;li&gt;An organization gets regularly raided by the security forces, who search all of the computers and mobile devices on the premises. The organization usually has at least a minute or two of warning before a raid starts. They need a very reliable way to trigger wiping all of the data from the sensitive apps.&lt;/li&gt;
	&lt;/ul&gt;


	&lt;ul&gt;
	&lt;li&gt;An aid worker has lots of sensitive data about people on their device. They regularly sync that data up with a secure, central database. Occasionally, the aid worker has to leave the country on very short notice. The border guards regularly download the entire contents of mobile devices of people crossing through. While waiting in line at the border, the aid worker sees the border guards seizing people's devices, and then remembers all the data on the device, so she unlocks her phone and hits the wipe trigger, which wipes all sensitive apps from the device. When the aid worker returns to the central office, the device is again synced up with the central database.&lt;/li&gt;
	&lt;/ul&gt;


	&lt;p&gt;This was started as part of the T2 Panic work, since sharing location is so often a part of panic apps. Follow our progress here:&lt;br /&gt;&lt;a class="external" href="https://guardianproject.info/tag/panic"&gt;https://guardianproject.info/tag/panic&lt;/a&gt;&lt;br /&gt;&lt;a class="external" href="https://dev.guardianproject.info/projects/panic"&gt;https://dev.guardianproject.info/projects/panic&lt;/a&gt;&lt;/p&gt;


	&lt;p&gt;Here is a video that demonstrates the user experience:&lt;br /&gt;&lt;a class="external" href="https://www.youtube.com/watch?v=mS1gstS6YS8"&gt;https://www.youtube.com/watch?v=mS1gstS6YS8&lt;/a&gt;&lt;/p&gt;    </content>
  </entry>
  <entry>
    <title>two example apps: FakePanicButton and FakePanicReceiver</title>
    <link rel="alternate" href="https://dev.guardianproject.info/news/210"/>
    <id>https://dev.guardianproject.info/news/210</id>
    <updated>2015-04-22T01:38:06Z</updated>
    <author>
      <name>hans</name>
      <email>hans@guardianproject.info</email>
    </author>
    <content type="html">
&lt;p&gt;I've publicly posted my two example panic apps: FakePanicButton and&lt;br /&gt;FakePanicReceiver. These are just meant to be simple examples of the panic&lt;br /&gt;framework in action.  I've been using them as my rapid prototyping platform&lt;br /&gt;before thinking about how to integrate this stuff into real apps.  Right now,&lt;br /&gt;you can see the list of "panic receivers" when you open FakePanicButton, and&lt;br /&gt;you can select them to receive the trigger that FakePanicButton sends.&lt;/p&gt;


	&lt;p&gt;On the FakePanicReceiver side, you can see the active panic trigger app, as&lt;br /&gt;well as the panic config screen, which is just a fullscreen placeholder with&lt;br /&gt;functional Cancel/Connect buttons.&lt;/p&gt;


Their git repos are on our GuardianProject organization:
	&lt;ul&gt;
	&lt;li&gt;&lt;a class="external" href="https://github.com/guardianproject/FakePanicButton"&gt;https://github.com/guardianproject/FakePanicButton&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a class="external" href="https://github.com/guardianproject/FakePanicReceiver"&gt;https://github.com/guardianproject/FakePanicReceiver&lt;/a&gt;&lt;/li&gt;
	&lt;/ul&gt;


and you can get test builds here:
	&lt;ul&gt;
	&lt;li&gt;&lt;a class="external" href="https://guardianproject.info/builds/FakePanicButton/latest"&gt;https://guardianproject.info/builds/FakePanicButton/latest&lt;/a&gt;&lt;/li&gt;
		&lt;li&gt;&lt;a class="external" href="https://guardianproject.info/builds/FakePanicReceiver/latest"&gt;https://guardianproject.info/builds/FakePanicReceiver/latest&lt;/a&gt;&lt;/li&gt;
	&lt;/ul&gt;


	&lt;p&gt;I've also been sketching things out in the panic wiki and issue tracker.  You&lt;br /&gt;can follow all the panic activity here:&lt;/p&gt;


	&lt;p&gt;&lt;a class="external" href="https://dev.guardianproject.info/projects/panic/activity"&gt;https://dev.guardianproject.info/projects/panic/activity&lt;/a&gt;&lt;/p&gt;    </content>
  </entry>
  <entry>
    <title>new app release: Location Privacy</title>
    <link rel="alternate" href="https://dev.guardianproject.info/news/202"/>
    <id>https://dev.guardianproject.info/news/202</id>
    <updated>2015-02-05T16:14:35Z</updated>
    <author>
      <name>hans</name>
      <email>hans@guardianproject.info</email>
    </author>
    <content type="html">
&lt;p&gt;LocationPrivacy is not really app but rather a set of "Intent Filters" for all of the various ways of sharing location. When you share location from one app, LocationPrivacy offers itself as an option. It then recognizes insecure methods of sharing location, and then converts them to more secure methods. This mostly means that it rewrites URLs to use https, and even to use `geo:` URIs, which can work on fully offline setups. LocationPrivacy mostly works by reading the location information from the URL itself. For many URLs, LocationPrivacy must actually load some of the webpage in order to get the location.&lt;/p&gt;


	&lt;p&gt;LocationPrivacy can also serve as a way to redirect all location links to your favorite mapping app. All map apps in Android can view `geo:` URIs, and LocationPrivacy converts many kinds of links to `geo:` URIs, including: Google Maps, OpenStreetMap, Amap, Baidu Map, QQ Map, Nokia HERE, Yandex Maps.&lt;/p&gt;


	&lt;p&gt;This is the first beta release of a new idea for us, if you have problems or ideas, please post them on our issue tracker so we can improve this app!&lt;br /&gt;https://dev.guardianproject.info/projects/panic/issues/new&lt;/p&gt;


	&lt;p&gt;This was started as part of the T2 Panic work, since sharing location is so often a part of panic apps. Follow our progress here:&lt;br /&gt;? &lt;a class="external" href="https://guardianproject.info/tag/panic"&gt;https://guardianproject.info/tag/panic&lt;/a&gt;&lt;br /&gt;? &lt;a class="external" href="https://dev.guardianproject.info/projects/panic"&gt;https://dev.guardianproject.info/projects/panic&lt;/a&gt;&lt;/p&gt;


	&lt;p&gt;Join us and help translate the app:&lt;br /&gt;&lt;a class="external" href="https://www.transifex.com/projects/p/locationprivacy"&gt;https://www.transifex.com/projects/p/locationprivacy&lt;/a&gt;&lt;/p&gt;    </content>
  </entry>
</feed>

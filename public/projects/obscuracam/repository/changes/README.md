<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>/README.md - Changes - ObscuraCam - Guardian Project Dev (ARCHIVED SITE)</title>
<meta name="description" content="Redmine" />
<meta name="keywords" content="issue,bug,tracker" />
<meta content="authenticity_token" name="csrf-param" />
<meta content="TFkt2Jobv7aDVO+oYsmsHbD4ygyDFiolu7KeLK56zU8=" name="csrf-token" />
<link rel='shortcut icon' href='../../../../favicon.ico%3F1371683577' />
<link href="../../../../stylesheets/jquery/jquery-ui-1.9.2.css%3F1371683503.css" media="all" rel="stylesheet" type="text/css" />
<link href="../../../../themes/a1/stylesheets/application.css%3F1386177785.css" media="all" rel="stylesheet" type="text/css" />

<script src="../../../../javascripts/jquery-1.8.3-ui-1.9.2-ujs-2.0.3.js%3F1371683503" type="text/javascript"></script>
<script src="../../../../javascripts/application.js%3F1371683577" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
$(window).load(function(){ warnLeavingUnsaved('The current page contains unsaved text that will be lost if you leave this page.'); });
//]]>
</script>


<!-- page specific tags -->
  <script src="../../../../javascripts/repository_navigation.js%3F1371683503" type="text/javascript"></script>
<link href="../../../../stylesheets/scm.css%3F1371683577.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body class="theme-A1 controller-repositories action-changes">
<div id="wrapper">
<div id="wrapper2">
<div id="wrapper3">
<div id="top-menu">
    <div id="account">
        <ul><li><a href="../../../../login.html" class="login">Sign in</a></li>
<li><a href="../../../../account/register.html" class="register">Register</a></li></ul>    </div>
    
    <ul><li><a href="../../../../index.html" class="home">Home</a></li>
<li><a href="../../../../projects.html" class="projects">Projects</a></li>
<li><a href="http://www.redmine.org/guide" class="help">Help</a></li></ul></div>

<div id="header">
    <div id="quick-search">
        <form accept-charset="UTF-8" action="../../search.html" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
        <input name="changesets" type="hidden" value="1" />
        <label for='q'>
          <a href="../../search.html" accesskey="4">Search</a>:
        </label>
        <input accesskey="f" class="small" id="q" name="q" size="20" type="text" />
</form>        
    </div>

    <h1><a href="../../../ssc%3Fjump=repository.html" class="root">SecureSmartCam</a> » ObscuraCam</h1>

    <div id="main-menu">
        <ul><li><a href="../../../obscuracam.html" class="overview">Overview</a></li>
<li><a href="../../activity.html" class="activity">Activity</a></li>
<li><a href="../../roadmap.html" class="roadmap">Roadmap</a></li>
<li><a href="../../issues.html" class="issues">Issues</a></li>
<li><a href="../../issues/new.html" accesskey="7" class="new-issue">New issue</a></li>
<li><a href="../../issues/gantt.html" class="gantt">Gantt</a></li>
<li><a href="../../issues/calendar.html" class="calendar">Calendar</a></li>
<li><a href="../../news.html" class="news">News</a></li>
<li><a href="../../files.html" class="files">Files</a></li>
<li><a href="../../repository.html" class="repository selected">Repository</a></li></ul>
    </div>
</div>

<div id="main" class="nosidebar">
    <div id="sidebar">
        
        
    </div>

    <div id="content">
        
        

<div class="contextual">
  
<a href="../statistics.html" class="icon icon-stats">Statistics</a>

<form accept-charset="UTF-8" action="https://dev.guardianproject.info/projects/obscuracam/repository/changes/README.md" id="revision_selector" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>  <!-- Branches Dropdown -->
    | Branch: 
    <select id="branch" name="branch"><option value=""></option>
<option value="PRGDemo">PRGDemo</option>
<option value="informav1">informav1</option>
<option value="jpegredaction">jpegredaction</option>
<option value="master" selected="selected">master</option>
<option value="n8fr8-sqlcipher">n8fr8-sqlcipher</option>
<option value="ninePatch">ninePatch</option>
<option value="obscurav1">obscurav1</option>
<option value="obscurav2">obscurav2</option>
<option value="tempHold">tempHold</option></select>

    | Tag: 
    <select id="tag" name="tag"><option value=""></option>
<option value="1.1-FINAL">1.1-FINAL</option>
<option value="1.2-FINAL">1.2-FINAL</option>
<option value="1.2-RC3">1.2-RC3</option>
<option value="2.0-Alpha-5">2.0-Alpha-5</option>
<option value="4.0.0-alpha-1">4.0.0-alpha-1</option>
<option value="4.0.0-alpha-2">4.0.0-alpha-2</option>
<option value="4.0.0-beta-1">4.0.0-beta-1</option>
<option value="v1.0.1">v1.0.1</option>
<option value="v1.0.2">v1.0.2</option>
<option value="v1.0.3">v1.0.3</option>
<option value="v1.0.4">v1.0.4</option>
<option value="v1.0.4.2">v1.0.4.2</option>
<option value="v1.1-beta">v1.1-beta</option>
<option value="v1.2">v1.2</option>
<option value="v1.2-RC1">v1.2-RC1</option>
<option value="v2-RC1">v2-RC1</option>
<option value="v2.0-RC2">v2.0-RC2</option>
<option value="v2.0-RC2b">v2.0-RC2b</option>
<option value="v4.0.0-alpha.38">v4.0.0-alpha.38</option></select>

    | Revision: 
    <input id="rev" name="rev" size="8" type="text" value="master" />
</form>
</div>

<h2><a href="../revisions/master/show.html">securesmartcam</a>
    / <a href="README.md%3Frev=master.html">README.md</a>
@ master

</h2>


<p>
History |
    <a href="https://dev.guardianproject.info/projects/obscuracam/repository/revisions/master/entry/README.md">View</a> |
    <a href="https://dev.guardianproject.info/projects/obscuracam/repository/revisions/master/annotate/README.md">Annotate</a> |
<a href="https://dev.guardianproject.info/projects/obscuracam/repository/revisions/master/raw/README.md">Download</a>
(337 Bytes)
</p>






<form accept-charset="UTF-8" action="https://dev.guardianproject.info/projects/obscuracam/repository/diff/README.md" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
<table class="list changesets">
<thead><tr>
<th>#</th>
<th></th>
<th></th>
<th>Date</th>
<th>Author</th>
<th>Comment</th>
</tr></thead>
<tbody>
<tr class="changeset odd">
<td class="id">
  <a href="../revisions/56b2c04dc0d571a7b770d6185fde29bd92b68c42.html" title="Revision 56b2c04d">56b2c04d</a>
</td><td class="checkbox"><input checked="checked" id="cb-1" name="rev" onclick="$(&#x27;#cbto-2&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="56b2c04dc0d571a7b770d6185fde29bd92b68c42" /></td>
<td class="checkbox"></td>
<td class="committed_on">08/08/2017 02:01 am</td>
<td class="author">n8fr8</td>
<td class="comments"><p>remove the old readme info</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/obscuracam/repository/revisions/5d1a3f7618c71d3a512a3bdc7abd5f69b237cb41" title="Revision 5d1a3f76">5d1a3f76</a>
</td><td class="checkbox"><input id="cb-2" name="rev" onclick="$(&#x27;#cbto-3&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="5d1a3f7618c71d3a512a3bdc7abd5f69b237cb41" /></td>
<td class="checkbox"><input checked="checked" id="cbto-2" name="rev_to" onclick="if ($(&#x27;#cb-2&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-1&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="5d1a3f7618c71d3a512a3bdc7abd5f69b237cb41" /></td>
<td class="committed_on">08/01/2017 07:09 pm</td>
<td class="author">n8fr8</td>
<td class="comments"><p>add support for additional masks through multi-tapping</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/obscuracam/repository/revisions/f7bc77c3b9377af061f687e1c923038a8d9505c8" title="Revision f7bc77c3">f7bc77c3</a>
</td><td class="checkbox"><input id="cb-3" name="rev" onclick="$(&#x27;#cbto-4&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="f7bc77c3b9377af061f687e1c923038a8d9505c8" /></td>
<td class="checkbox"><input id="cbto-3" name="rev_to" onclick="if ($(&#x27;#cb-3&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-2&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="f7bc77c3b9377af061f687e1c923038a8d9505c8" /></td>
<td class="committed_on">02/27/2014 07:55 pm</td>
<td class="author">Martin Ranta </td>
<td class="comments"><p>Update README.md</p>


	<p>The link was dead. I replaced it with a working one.</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/obscuracam/repository/revisions/2a2cbe158b9d6b343c9b259fef7c217647ffd5b7" title="Revision 2a2cbe15">2a2cbe15</a>
</td><td class="checkbox"><input id="cb-4" name="rev" onclick="$(&#x27;#cbto-5&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="2a2cbe158b9d6b343c9b259fef7c217647ffd5b7" /></td>
<td class="checkbox"><input id="cbto-4" name="rev_to" onclick="if ($(&#x27;#cb-4&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-3&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="2a2cbe158b9d6b343c9b259fef7c217647ffd5b7" /></td>
<td class="committed_on">08/30/2013 11:07 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>add instructions for building from the terminal</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/obscuracam/repository/revisions/5c4c202ed44a85842326dd94585f1f4e3993fba9" title="Revision 5c4c202e">5c4c202e</a>
</td><td class="checkbox"></td>
<td class="checkbox"><input id="cbto-5" name="rev_to" onclick="if ($(&#x27;#cb-5&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-4&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="5c4c202ed44a85842326dd94585f1f4e3993fba9" /></td>
<td class="committed_on">08/30/2013 10:33 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>make README markdown format</p></td>
</tr>
</tbody>
</table>
<input type="submit" value="View differences" />
</form>



        
        <div style="clear:both;"></div>
    </div>
</div>
</div>

<div id="ajax-indicator" style="display:none;"><span>Loading...</span></div>
<div id="ajax-modal" style="display:none;"></div>

<div id="footer">
  <div class="bgl"><div class="bgr">
    Powered by <a href="http://www.redmine.org/">Redmine</a> &copy; 2006-2013 Jean-Philippe Lang
  </div></div>
</div>
</div>
</div>

</body>
</html>

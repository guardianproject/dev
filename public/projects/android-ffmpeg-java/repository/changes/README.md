<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>/README.md - Changes - android-ffmpeg-java - Guardian Project Dev (ARCHIVED SITE)</title>
<meta name="description" content="Redmine" />
<meta name="keywords" content="issue,bug,tracker" />
<meta content="authenticity_token" name="csrf-param" />
<meta content="TFkt2Jobv7aDVO+oYsmsHbD4ygyDFiolu7KeLK56zU8=" name="csrf-token" />
<link rel='shortcut icon' href='../../../../favicon.ico%3F1371683577' />
<link href="../../../../stylesheets/jquery/jquery-ui-1.9.2.css%3F1371683503.css" media="all" rel="stylesheet" type="text/css" />
<link href="../../../../themes/a1/stylesheets/application.css%3F1386177785.css" media="all" rel="stylesheet" type="text/css" />

<script src="../../../../javascripts/jquery-1.8.3-ui-1.9.2-ujs-2.0.3.js%3F1371683503" type="text/javascript"></script>
<script src="../../../../javascripts/application.js%3F1371683577" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
$(window).load(function(){ warnLeavingUnsaved('The current page contains unsaved text that will be lost if you leave this page.'); });
//]]>
</script>


<!-- page specific tags -->
  <script src="../../../../javascripts/repository_navigation.js%3F1371683503" type="text/javascript"></script>
<link href="../../../../stylesheets/scm.css%3F1371683577.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body class="theme-A1 controller-repositories action-changes">
<div id="wrapper">
<div id="wrapper2">
<div id="wrapper3">
<div id="top-menu">
    <div id="account">
        <ul><li><a href="../../../../login.html" class="login">Sign in</a></li>
<li><a href="../../../../account/register.html" class="register">Register</a></li></ul>    </div>
    
    <ul><li><a href="../../../../index.html" class="home">Home</a></li>
<li><a href="../../../../projects.html" class="projects">Projects</a></li>
<li><a href="http://www.redmine.org/guide" class="help">Help</a></li></ul></div>

<div id="header">
    <div id="quick-search">
        <form accept-charset="UTF-8" action="../../search.html" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
        <input name="changesets" type="hidden" value="1" />
        <label for='q'>
          <a href="../../search.html" accesskey="4">Search</a>:
        </label>
        <input accesskey="f" class="small" id="q" name="q" size="20" type="text" />
</form>        
    </div>

    <h1><a href="../../../libs%3Fjump=repository.html" class="root">Developer Libraries</a> » android-ffmpeg-java</h1>

    <div id="main-menu">
        <ul><li><a href="../../../android-ffmpeg-java.html" class="overview">Overview</a></li>
<li><a href="../../activity.html" class="activity">Activity</a></li>
<li><a href="../../issues.html" class="issues">Issues</a></li>
<li><a href="../../repository.html" class="repository selected">Repository</a></li></ul>
    </div>
</div>

<div id="main" class="nosidebar">
    <div id="sidebar">
        
        
    </div>

    <div id="content">
        
        

<div class="contextual">
  
<a href="../statistics.html" class="icon icon-stats">Statistics</a>

<form accept-charset="UTF-8" action="https://dev.guardianproject.info/projects/android-ffmpeg-java/repository/changes/README.md" id="revision_selector" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>  <!-- Branches Dropdown -->
    | Branch: 
    <select id="branch" name="branch"><option value=""></option>
<option value="master" selected="selected">master</option></select>


    | Revision: 
    <input id="rev" name="rev" size="8" type="text" value="master" />
</form>
</div>

<h2><a href="../revisions/master/show.html">android-ffmpeg-java</a>
    / <a href="README.md%3Frev=master.html">README.md</a>
@ master

</h2>


<p>
History |
    <a href="https://dev.guardianproject.info/projects/android-ffmpeg-java/repository/revisions/master/entry/README.md">View</a> |
    <a href="https://dev.guardianproject.info/projects/android-ffmpeg-java/repository/revisions/master/annotate/README.md">Annotate</a> |
<a href="https://dev.guardianproject.info/projects/android-ffmpeg-java/repository/revisions/master/raw/README.md">Download</a>
(969 Bytes)
</p>






<form accept-charset="UTF-8" action="https://dev.guardianproject.info/projects/android-ffmpeg-java/repository/diff/README.md" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
<table class="list changesets">
<thead><tr>
<th>#</th>
<th></th>
<th></th>
<th>Date</th>
<th>Author</th>
<th>Comment</th>
</tr></thead>
<tbody>
<tr class="changeset odd">
<td class="id">
  <a href="../revisions/7eab4b3bc0d194c8c2b73844974ff74d8c53ba8a.html" title="Revision 7eab4b3b">7eab4b3b</a>
</td><td class="checkbox"><input checked="checked" id="cb-1" name="rev" onclick="$(&#x27;#cbto-2&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="7eab4b3bc0d194c8c2b73844974ff74d8c53ba8a" /></td>
<td class="checkbox"></td>
<td class="committed_on">11/17/2013 03:18 am</td>
<td class="author">n8fr8</td>
<td class="comments"><p>updating README.md and removing readme file from res/raw</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/android-ffmpeg-java/repository/revisions/d977296cf23eeadce66dede7fbf039ccd8f6590c" title="Revision d977296c">d977296c</a>
</td><td class="checkbox"><input id="cb-2" name="rev" onclick="$(&#x27;#cbto-3&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="d977296cf23eeadce66dede7fbf039ccd8f6590c" /></td>
<td class="checkbox"><input checked="checked" id="cbto-2" name="rev_to" onclick="if ($(&#x27;#cb-2&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-1&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="d977296cf23eeadce66dede7fbf039ccd8f6590c" /></td>
<td class="committed_on">11/17/2013 02:45 am</td>
<td class="author">n8fr8</td>
<td class="comments"><p>adding in the built binaries for developers who don't have the<br />NDK and/or or not on Linux (Mac, Windows, etc)</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="../revisions/b2d5fd4ebb18a0b546665850c63780e2706529d2.html" title="Revision b2d5fd4e">b2d5fd4e</a>
</td><td class="checkbox"><input id="cb-3" name="rev" onclick="$(&#x27;#cbto-4&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="b2d5fd4ebb18a0b546665850c63780e2706529d2" /></td>
<td class="checkbox"><input id="cbto-3" name="rev_to" onclick="if ($(&#x27;#cb-3&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-2&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="b2d5fd4ebb18a0b546665850c63780e2706529d2" /></td>
<td class="committed_on">02/28/2013 12:38 pm</td>
<td class="author">Abel Luck </td>
<td class="comments"><p>Add license info <a href="../../../../issues/5.html" class="issue tracker-2 status-5 priority-6 priority-high2 closed" title="Get Website Online (Closed)">#5</a></p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/android-ffmpeg-java/repository/revisions/7bf71d1eb8d1d22abad3ab0115a8682125cbea36" title="Revision 7bf71d1e">7bf71d1e</a>
</td><td class="checkbox"></td>
<td class="checkbox"><input id="cbto-4" name="rev_to" onclick="if ($(&#x27;#cb-4&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-3&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="7bf71d1eb8d1d22abad3ab0115a8682125cbea36" /></td>
<td class="committed_on">09/12/2012 06:31 pm</td>
<td class="author">Abel Luck </td>
<td class="comments"><p>Update readme with better instructions</p></td>
</tr>
</tbody>
</table>
<input type="submit" value="View differences" />
</form>



        
        <div style="clear:both;"></div>
    </div>
</div>
</div>

<div id="ajax-indicator" style="display:none;"><span>Loading...</span></div>
<div id="ajax-modal" style="display:none;"></div>

<div id="footer">
  <div class="bgl"><div class="bgr">
    Powered by <a href="http://www.redmine.org/">Redmine</a> &copy; 2006-2013 Jean-Philippe Lang
  </div></div>
</div>
</div>
</div>

</body>
</html>

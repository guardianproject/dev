<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>/content.py - Changes - StoryMaker™ - Guardian Project Dev (ARCHIVED SITE)</title>
<meta name="description" content="Redmine" />
<meta name="keywords" content="issue,bug,tracker" />
<meta content="authenticity_token" name="csrf-param" />
<meta content="TFkt2Jobv7aDVO+oYsmsHbD4ygyDFiolu7KeLK56zU8=" name="csrf-token" />
<link rel='shortcut icon' href='/favicon.ico?1371683577' />
<link href="/stylesheets/jquery/jquery-ui-1.9.2.css?1371683503" media="all" rel="stylesheet" type="text/css" />
<link href="/themes/a1/stylesheets/application.css?1386177785" media="all" rel="stylesheet" type="text/css" />

<script src="/javascripts/jquery-1.8.3-ui-1.9.2-ujs-2.0.3.js?1371683503" type="text/javascript"></script>
<script src="/javascripts/application.js?1371683577" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
$(window).load(function(){ warnLeavingUnsaved('The current page contains unsaved text that will be lost if you leave this page.'); });
//]]>
</script>


<!-- page specific tags -->
  <script src="/javascripts/repository_navigation.js?1371683503" type="text/javascript"></script>
<link href="/stylesheets/scm.css?1371683577" media="screen" rel="stylesheet" type="text/css" />
</head>
<body class="theme-A1 controller-repositories action-changes">
<div id="wrapper">
<div id="wrapper2">
<div id="wrapper3">
<div id="top-menu">
    <div id="account">
        <ul><li><a href="/login" class="login">Sign in</a></li>
<li><a href="/account/register" class="register">Register</a></li></ul>    </div>
    
    <ul><li><a href="/" class="home">Home</a></li>
<li><a href="/projects" class="projects">Projects</a></li>
<li><a href="http://www.redmine.org/guide" class="help">Help</a></li></ul></div>

<div id="header">
    <div id="quick-search">
        <form accept-charset="UTF-8" action="/projects/wrapp/search" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
        <input name="changesets" type="hidden" value="1" />
        <label for='q'>
          <a href="/projects/wrapp/search" accesskey="4">Search</a>:
        </label>
        <input accesskey="f" class="small" id="q" name="q" size="20" type="text" />
</form>        
    </div>

    <h1>StoryMaker™</h1>

    <div id="main-menu">
        <ul><li><a href="/projects/wrapp" class="overview">Overview</a></li>
<li><a href="/projects/wrapp/activity" class="activity">Activity</a></li>
<li><a href="/projects/wrapp/roadmap" class="roadmap">Roadmap</a></li>
<li><a href="/projects/wrapp/issues" class="issues">Issues</a></li>
<li><a href="/projects/wrapp/issues/new" accesskey="7" class="new-issue">New issue</a></li>
<li><a href="/projects/wrapp/issues/gantt" class="gantt">Gantt</a></li>
<li><a href="/projects/wrapp/issues/calendar" class="calendar">Calendar</a></li>
<li><a href="/projects/wrapp/news" class="news">News</a></li>
<li><a href="/projects/wrapp/documents" class="documents">Documents</a></li>
<li><a href="/projects/wrapp/wiki" class="wiki">Wiki</a></li>
<li><a href="/projects/wrapp/boards" class="boards">Forums</a></li>
<li><a href="/projects/wrapp/files" class="files">Files</a></li>
<li><a href="/projects/wrapp/repository" class="repository selected">Repository</a></li></ul>
    </div>
</div>

<div id="main" class="nosidebar">
    <div id="sidebar">
        
        
    </div>

    <div id="content">
        
        

<div class="contextual">
  
<a href="/projects/wrapp/repository/statistics" class="icon icon-stats">Statistics</a>

<form accept-charset="UTF-8" action="/projects/wrapp/repository/changes/content.py" id="revision_selector" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>  <!-- Branches Dropdown -->
    | Branch: 
    <select id="branch" name="branch"><option value=""></option>
<option value="feature-101-interactive-trainer">feature-101-interactive-trainer</option>
<option value="lesson-dev">lesson-dev</option>
<option value="master" selected="selected">master</option>
<option value="server-dev">server-dev</option>
<option value="ui-dev">ui-dev</option></select>

    | Tag: 
    <select id="tag" name="tag"><option value=""></option>
<option value="0.0.10-build118">0.0.10-build118</option>
<option value="0.0.2-dev-build12">0.0.2-dev-build12</option>
<option value="0.0.2-dev-build17">0.0.2-dev-build17</option>
<option value="0.0.2-dev-build21">0.0.2-dev-build21</option>
<option value="0.0.2.a2">0.0.2.a2</option>
<option value="0.0.5-alpha-1">0.0.5-alpha-1</option>
<option value="0.0.5-beta1-build59">0.0.5-beta1-build59</option>
<option value="0.0.5-beta1-build68">0.0.5-beta1-build68</option>
<option value="0.0.5-beta1-build75">0.0.5-beta1-build75</option>
<option value="0.0.5-beta1-build82">0.0.5-beta1-build82</option>
<option value="0.0.5-build53-beta1">0.0.5-build53-beta1</option>
<option value="0.0.6-beta3-build90-rc1">0.0.6-beta3-build90-rc1</option>
<option value="0.0.6-beta3-build91-rc2">0.0.6-beta3-build91-rc2</option>
<option value="0.0.7-build95">0.0.7-build95</option>
<option value="0.0.8-beta-1">0.0.8-beta-1</option>
<option value="0.0.8-build96">0.0.8-build96</option>
<option value="0.0.9-build112">0.0.9-build112</option>
<option value="0.0.9-build114.2">0.0.9-build114.2</option>
<option value="0.0.9-build115">0.0.9-build115</option>
<option value="0.0.9-build116">0.0.9-build116</option>
<option value="2.0.0-build1003">2.0.0-build1003</option>
<option value="2.0.0-build1004">2.0.0-build1004</option>
<option value="2.0.0-build1005">2.0.0-build1005</option>
<option value="2.0.0-build1006">2.0.0-build1006</option>
<option value="2.0.0-build1008">2.0.0-build1008</option>
<option value="2.0.0-build1009">2.0.0-build1009</option>
<option value="2.0.0-build1010">2.0.0-build1010</option>
<option value="2.0.0-build1011">2.0.0-build1011</option>
<option value="2.0.0-build1012">2.0.0-build1012</option>
<option value="2.0.0-build1013">2.0.0-build1013</option>
<option value="2.0.0-build1014">2.0.0-build1014</option>
<option value="2.0.0-build1015">2.0.0-build1015</option>
<option value="2.0.0-build1016">2.0.0-build1016</option>
<option value="2.0.0-build1017">2.0.0-build1017</option>
<option value="2.0.0-build1017.2">2.0.0-build1017.2</option>
<option value="2.0.0-build1018">2.0.0-build1018</option>
<option value="2.0.0-build1019">2.0.0-build1019</option>
<option value="2.0.0-build1021">2.0.0-build1021</option>
<option value="2.0.0-build1023">2.0.0-build1023</option>
<option value="2.0.0-build1026">2.0.0-build1026</option>
<option value="2.0.0-build1027">2.0.0-build1027</option>
<option value="2.0.0-build1028">2.0.0-build1028</option>
<option value="2.0.0-build1029">2.0.0-build1029</option>
<option value="2.0.0-build1030">2.0.0-build1030</option>
<option value="2.0.0-build1032">2.0.0-build1032</option>
<option value="2.0.0-build1034">2.0.0-build1034</option>
<option value="2.0.0-build1035">2.0.0-build1035</option>
<option value="2.0.0-build1036">2.0.0-build1036</option>
<option value="2.0.0-build1038">2.0.0-build1038</option>
<option value="2.0.0-build1039">2.0.0-build1039</option>
<option value="2.0.0-build1041">2.0.0-build1041</option>
<option value="2.0.0-build1043">2.0.0-build1043</option>
<option value="2.0.0-build1044">2.0.0-build1044</option>
<option value="2.0.0-build1045">2.0.0-build1045</option>
<option value="2.0.0-build1046">2.0.0-build1046</option>
<option value="2.0.0-build1047">2.0.0-build1047</option>
<option value="2.0.0-build1048">2.0.0-build1048</option>
<option value="2.0.0-build1050">2.0.0-build1050</option>
<option value="2.0.0-build1051">2.0.0-build1051</option>
<option value="2.0.0-build1052">2.0.0-build1052</option>
<option value="2.0.0-build1052.2">2.0.0-build1052.2</option>
<option value="2.0.0-build1056">2.0.0-build1056</option>
<option value="2.0.0-build1058">2.0.0-build1058</option>
<option value="2.0.1-build1064">2.0.1-build1064</option>
<option value="2.0.1-build1065">2.0.1-build1065</option>
<option value="2.0.3-build1068">2.0.3-build1068</option>
<option value="2.0.4-build1069">2.0.4-build1069</option>
<option value="2.0.5-build1079">2.0.5-build1079</option>
<option value="2.1.4">2.1.4</option>
<option value="2.1.6.1">2.1.6.1</option>
<option value="2.1.6.1-dev">2.1.6.1-dev</option>
<option value="2.1.6.10">2.1.6.10</option>
<option value="2.1.6.2">2.1.6.2</option>
<option value="2.1.6.3">2.1.6.3</option>
<option value="2.1.6.4">2.1.6.4</option>
<option value="2.1.6.5">2.1.6.5</option>
<option value="2.1.6.6">2.1.6.6</option>
<option value="2.1.6.7-RC-1">2.1.6.7-RC-1</option>
<option value="2.1.6.7-beta-1">2.1.6.7-beta-1</option>
<option value="2.1.6.7-beta-2">2.1.6.7-beta-2</option>
<option value="2.1.6.8-BETA-1">2.1.6.8-BETA-1</option>
<option value="2.1.6.9">2.1.6.9</option>
<option value="build1070">build1070</option>
<option value="build1071">build1071</option>
<option value="build1072">build1072</option>
<option value="build1074">build1074</option>
<option value="build1077">build1077</option>
<option value="build1080">build1080</option>
<option value="build1081">build1081</option>
<option value="build1082">build1082</option>
<option value="build1088">build1088</option>
<option value="build1090">build1090</option>
<option value="build1091">build1091</option>
<option value="build1094">build1094</option>
<option value="build1096">build1096</option>
<option value="build1097">build1097</option>
<option value="build1099">build1099</option>
<option value="build1103">build1103</option>
<option value="v2.0.5">v2.0.5</option>
<option value="v2.0.6">v2.0.6</option>
<option value="v2.1.1">v2.1.1</option>
<option value="v2.1.3">v2.1.3</option></select>

    | Revision: 
    <input id="rev" name="rev" size="8" type="text" value="master" />
</form>
</div>

<h2><a href="/projects/wrapp/repository/revisions/master/show">storymaker</a>
    / <a href="/projects/wrapp/repository/changes/content.py?rev=master">content.py</a>
@ master

</h2>


<p>
History |
    <a href="/projects/wrapp/repository/revisions/master/entry/content.py">View</a> |
    <a href="/projects/wrapp/repository/revisions/master/annotate/content.py">Annotate</a> |
<a href="/projects/wrapp/repository/revisions/master/raw/content.py">Download</a>
(12.4 KB)
</p>






<form accept-charset="UTF-8" action="/projects/wrapp/repository/diff/content.py" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
<table class="list changesets">
<thead><tr>
<th>#</th>
<th></th>
<th></th>
<th>Date</th>
<th>Author</th>
<th>Comment</th>
</tr></thead>
<tbody>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/bec1eaf27c210d670a78d39001ac31429852669a" title="Revision bec1eaf2">bec1eaf2</a>
</td><td class="checkbox"><input checked="checked" id="cb-1" name="rev" onclick="$(&#x27;#cbto-2&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="bec1eaf27c210d670a78d39001ac31429852669a" /></td>
<td class="checkbox"></td>
<td class="committed_on">01/16/2016 05:09 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>update content.py</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/b25a52676e8a2e8ffc7c426fb170b7127c6d9e95" title="Revision b25a5267">b25a5267</a>
</td><td class="checkbox"><input id="cb-2" name="rev" onclick="$(&#x27;#cbto-3&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="b25a52676e8a2e8ffc7c426fb170b7127c6d9e95" /></td>
<td class="checkbox"><input checked="checked" id="cbto-2" name="rev_to" onclick="if ($(&#x27;#cb-2&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-1&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="b25a52676e8a2e8ffc7c426fb170b7127c6d9e95" /></td>
<td class="committed_on">11/04/2015 02:08 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>cleanup</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/7477a56932f9b08d4489bad1aba7bb38c5807274" title="Revision 7477a569">7477a569</a>
</td><td class="checkbox"><input id="cb-3" name="rev" onclick="$(&#x27;#cbto-4&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="7477a56932f9b08d4489bad1aba7bb38c5807274" /></td>
<td class="checkbox"><input id="cbto-3" name="rev_to" onclick="if ($(&#x27;#cb-3&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-2&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="7477a56932f9b08d4489bad1aba7bb38c5807274" /></td>
<td class="committed_on">09/30/2015 09:24 pm</td>
<td class="author">vitriolix</td>
<td class="comments"><p>fix for content.py test command</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/4cc19e5460b4155f89338d0e2d98e8ca281109b4" title="Revision 4cc19e54">4cc19e54</a>
</td><td class="checkbox"><input id="cb-4" name="rev" onclick="$(&#x27;#cbto-5&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="4cc19e5460b4155f89338d0e2d98e8ca281109b4" /></td>
<td class="checkbox"><input id="cbto-4" name="rev_to" onclick="if ($(&#x27;#cb-4&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-3&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="4cc19e5460b4155f89338d0e2d98e8ca281109b4" /></td>
<td class="committed_on">09/30/2015 03:09 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>fix push_test_files command to work with new sqlbrite indexes</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/a6747e84fded040821ed55d781e4c6f643618d1c" title="Revision a6747e84">a6747e84</a>
</td><td class="checkbox"><input id="cb-5" name="rev" onclick="$(&#x27;#cbto-6&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="a6747e84fded040821ed55d781e4c6f643618d1c" /></td>
<td class="checkbox"><input id="cbto-5" name="rev_to" onclick="if ($(&#x27;#cb-5&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-4&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="a6747e84fded040821ed55d781e4c6f643618d1c" /></td>
<td class="committed_on">09/29/2015 03:19 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>bump to v2.0.5 build 1078; update Mobile Photo 101 pack</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/c59baf540526a4492dccc5233f972b362f0ff274" title="Revision c59baf54">c59baf54</a>
</td><td class="checkbox"><input id="cb-6" name="rev" onclick="$(&#x27;#cbto-7&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="c59baf540526a4492dccc5233f972b362f0ff274" /></td>
<td class="checkbox"><input id="cbto-6" name="rev_to" onclick="if ($(&#x27;#cb-6&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-5&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="c59baf540526a4492dccc5233f972b362f0ff274" /></td>
<td class="committed_on">09/21/2015 12:31 pm</td>
<td class="author">vitriolix</td>
<td class="comments"><p>update content.py</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/99f7bfc04169cccde331fc5a8541d2d9bff66425" title="Revision 99f7bfc0">99f7bfc0</a>
</td><td class="checkbox"><input id="cb-7" name="rev" onclick="$(&#x27;#cbto-8&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="99f7bfc04169cccde331fc5a8541d2d9bff66425" /></td>
<td class="checkbox"><input id="cbto-7" name="rev_to" onclick="if ($(&#x27;#cb-7&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-6&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="99f7bfc04169cccde331fc5a8541d2d9bff66425" /></td>
<td class="committed_on">09/09/2015 11:44 pm</td>
<td class="author">vitriolix</td>
<td class="comments"><p>refactor content.py script to be easier to use for building and testing packs</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/7f3a748346c986e99675f1c66540ca4ff6c8f260" title="Revision 7f3a7483">7f3a7483</a>
</td><td class="checkbox"><input id="cb-8" name="rev" onclick="$(&#x27;#cbto-9&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="7f3a748346c986e99675f1c66540ca4ff6c8f260" /></td>
<td class="checkbox"><input id="cbto-8" name="rev_to" onclick="if ($(&#x27;#cb-8&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-7&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="7f3a748346c986e99675f1c66540ca4ff6c8f260" /></td>
<td class="committed_on">08/25/2015 05:07 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>fix params to zip</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/e950d7e593c5123768572ab89c3443b35191fa36" title="Revision e950d7e5">e950d7e5</a>
</td><td class="checkbox"><input id="cb-9" name="rev" onclick="$(&#x27;#cbto-10&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="e950d7e593c5123768572ab89c3443b35191fa36" /></td>
<td class="checkbox"><input id="cbto-9" name="rev_to" onclick="if ($(&#x27;#cb-9&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-8&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="e950d7e593c5123768572ab89c3443b35191fa36" /></td>
<td class="committed_on">07/14/2015 02:05 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>Merge branch 'demo' into swoose</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/e2568c0b07133cbc8f9671ef86d275bf17b63a41" title="Revision e2568c0b">e2568c0b</a>
</td><td class="checkbox"><input id="cb-10" name="rev" onclick="$(&#x27;#cbto-11&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="e2568c0b07133cbc8f9671ef86d275bf17b63a41" /></td>
<td class="checkbox"><input id="cbto-10" name="rev_to" onclick="if ($(&#x27;#cb-10&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-9&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="e2568c0b07133cbc8f9671ef86d275bf17b63a41" /></td>
<td class="committed_on">07/14/2015 01:26 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>rename cuba pack to mobile photo 101</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/a73324e86806e29a6c4ff0b0ed306b40671335c9" title="Revision a73324e8">a73324e8</a>
</td><td class="checkbox"><input id="cb-11" name="rev" onclick="$(&#x27;#cbto-12&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="a73324e86806e29a6c4ff0b0ed306b40671335c9" /></td>
<td class="checkbox"><input id="cbto-11" name="rev_to" onclick="if ($(&#x27;#cb-11&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-10&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="a73324e86806e29a6c4ff0b0ed306b40671335c9" /></td>
<td class="committed_on">07/02/2015 04:36 pm</td>
<td class="author">vitriolix</td>
<td class="comments"><p>add cuba pack to content.py</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/d1e780a0c67366ecc76a0eff7110bd30b534deb3" title="Revision d1e780a0">d1e780a0</a>
</td><td class="checkbox"><input id="cb-12" name="rev" onclick="$(&#x27;#cbto-13&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="d1e780a0c67366ecc76a0eff7110bd30b534deb3" /></td>
<td class="checkbox"><input id="cbto-12" name="rev_to" onclick="if ($(&#x27;#cb-12&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-11&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="d1e780a0c67366ecc76a0eff7110bd30b534deb3" /></td>
<td class="committed_on">06/19/2015 09:57 pm</td>
<td class="author">vitriolix</td>
<td class="comments"><p>exclude more media file extensions from zip compression in content.py</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/1b726ece9c786d85ed950f1de0163fb817da3ccc" title="Revision 1b726ece">1b726ece</a>
</td><td class="checkbox"><input id="cb-13" name="rev" onclick="$(&#x27;#cbto-14&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="1b726ece9c786d85ed950f1de0163fb817da3ccc" /></td>
<td class="checkbox"><input id="cbto-13" name="rev_to" onclick="if ($(&#x27;#cb-13&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-12&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="1b726ece9c786d85ed950f1de0163fb817da3ccc" /></td>
<td class="committed_on">06/06/2015 02:11 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>update build 1056</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/c8263c88457d45ebc1812f968c6830e65fb4c4a1" title="Revision c8263c88">c8263c88</a>
</td><td class="checkbox"><input id="cb-14" name="rev" onclick="$(&#x27;#cbto-15&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="c8263c88457d45ebc1812f968c6830e65fb4c4a1" /></td>
<td class="checkbox"><input id="cbto-14" name="rev_to" onclick="if ($(&#x27;#cb-14&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-13&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="c8263c88457d45ebc1812f968c6830e65fb4c4a1" /></td>
<td class="committed_on">06/04/2015 03:19 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>update content.py to newest beta pack build</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/71e6379c5eeaefcd8c396e32e24be60d942f9e90" title="Revision 71e6379c">71e6379c</a>
</td><td class="checkbox"><input id="cb-15" name="rev" onclick="$(&#x27;#cbto-16&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="71e6379c5eeaefcd8c396e32e24be60d942f9e90" /></td>
<td class="checkbox"><input id="cbto-15" name="rev_to" onclick="if ($(&#x27;#cb-15&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-14&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="71e6379c5eeaefcd8c396e32e24be60d942f9e90" /></td>
<td class="committed_on">06/03/2015 08:52 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>update content.py for new beta pack</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/e4d8094a0f752a496dd096d20cf048896639eb98" title="Revision e4d8094a">e4d8094a</a>
</td><td class="checkbox"><input id="cb-16" name="rev" onclick="$(&#x27;#cbto-17&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="e4d8094a0f752a496dd096d20cf048896639eb98" /></td>
<td class="checkbox"><input id="cbto-16" name="rev_to" onclick="if ($(&#x27;#cb-16&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-15&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="e4d8094a0f752a496dd096d20cf048896639eb98" /></td>
<td class="committed_on">05/27/2015 06:57 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>fix beta cover filename; update content script to fix beta pack generation</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/c09289aed237cd8306b19d6d0d38bf496a1eb76d" title="Revision c09289ae">c09289ae</a>
</td><td class="checkbox"><input id="cb-17" name="rev" onclick="$(&#x27;#cbto-18&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="c09289aed237cd8306b19d6d0d38bf496a1eb76d" /></td>
<td class="checkbox"><input id="cbto-17" name="rev_to" onclick="if ($(&#x27;#cb-17&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-16&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="c09289aed237cd8306b19d6d0d38bf496a1eb76d" /></td>
<td class="committed_on">05/27/2015 02:23 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>add beta content pack</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/be7a7812798deedaab3ad90839d2a9b737a3a579" title="Revision be7a7812">be7a7812</a>
</td><td class="checkbox"><input id="cb-18" name="rev" onclick="$(&#x27;#cbto-19&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="be7a7812798deedaab3ad90839d2a9b737a3a579" /></td>
<td class="checkbox"><input id="cbto-18" name="rev_to" onclick="if ($(&#x27;#cb-18&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-17&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="be7a7812798deedaab3ad90839d2a9b737a3a579" /></td>
<td class="committed_on">04/15/2015 07:49 pm</td>
<td class="author">vitriolix</td>
<td class="comments"><p>bump to build 1047</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/721c7d9ead480c28f53bcb53ad1743884b8f1ce8" title="Revision 721c7d9e">721c7d9e</a>
</td><td class="checkbox"><input id="cb-19" name="rev" onclick="$(&#x27;#cbto-20&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="721c7d9ead480c28f53bcb53ad1743884b8f1ce8" /></td>
<td class="checkbox"><input id="cbto-19" name="rev_to" onclick="if ($(&#x27;#cb-19&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-18&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="721c7d9ead480c28f53bcb53ad1743884b8f1ce8" /></td>
<td class="committed_on">04/15/2015 07:39 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>update content.py for ljf15 v3</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/3bc471ff79a588dd55acf8814896ae2a39d8d345" title="Revision 3bc471ff">3bc471ff</a>
</td><td class="checkbox"><input id="cb-20" name="rev" onclick="$(&#x27;#cbto-21&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="3bc471ff79a588dd55acf8814896ae2a39d8d345" /></td>
<td class="checkbox"><input id="cbto-20" name="rev_to" onclick="if ($(&#x27;#cb-20&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-19&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="3bc471ff79a588dd55acf8814896ae2a39d8d345" /></td>
<td class="committed_on">04/15/2015 02:28 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>update pegs; bump to build 1045</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/6e478f40f9d6b5ecf27dcc59ea5dc654a19086b3" title="Revision 6e478f40">6e478f40</a>
</td><td class="checkbox"><input id="cb-21" name="rev" onclick="$(&#x27;#cbto-22&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="6e478f40f9d6b5ecf27dcc59ea5dc654a19086b3" /></td>
<td class="checkbox"><input id="cbto-21" name="rev_to" onclick="if ($(&#x27;#cb-21&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-20&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="6e478f40f9d6b5ecf27dcc59ea5dc654a19086b3" /></td>
<td class="committed_on">04/13/2015 07:06 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>update content.py to make debugging ijf15 pack easier</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/780f46f4c2edecee253a7fac8a8ae3451ce71970" title="Revision 780f46f4">780f46f4</a>
</td><td class="checkbox"><input id="cb-22" name="rev" onclick="$(&#x27;#cbto-23&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="780f46f4c2edecee253a7fac8a8ae3451ce71970" /></td>
<td class="checkbox"><input id="cbto-22" name="rev_to" onclick="if ($(&#x27;#cb-22&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-21&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="780f46f4c2edecee253a7fac8a8ae3451ce71970" /></td>
<td class="committed_on">04/12/2015 03:08 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>bump ijf15 content pack to v2</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/16ff25c52596ed6592fbc17be7f519868c2f05b4" title="Revision 16ff25c5">16ff25c5</a>
</td><td class="checkbox"><input id="cb-23" name="rev" onclick="$(&#x27;#cbto-24&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="16ff25c52596ed6592fbc17be7f519868c2f05b4" /></td>
<td class="checkbox"><input id="cbto-23" name="rev_to" onclick="if ($(&#x27;#cb-23&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-22&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="16ff25c52596ed6592fbc17be7f519868c2f05b4" /></td>
<td class="committed_on">04/11/2015 10:12 pm</td>
<td class="author">vitriolix</td>
<td class="comments"><p>adding IJF15 path to content.py</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/1d288a7242ecaeca0d3a590d7cb7c913165885cd" title="Revision 1d288a72">1d288a72</a>
</td><td class="checkbox"><input id="cb-24" name="rev" onclick="$(&#x27;#cbto-25&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="1d288a7242ecaeca0d3a590d7cb7c913165885cd" /></td>
<td class="checkbox"><input id="cbto-24" name="rev_to" onclick="if ($(&#x27;#cb-24&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-23&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="1d288a7242ecaeca0d3a590d7cb7c913165885cd" /></td>
<td class="committed_on">03/29/2015 04:46 pm</td>
<td class="author">vitriolix</td>
<td class="comments"><p>updating content.py script</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/da9804f3ac9c8f69bdc0497bff3002e8af5a4a58" title="Revision da9804f3">da9804f3</a>
</td><td class="checkbox"><input id="cb-25" name="rev" onclick="$(&#x27;#cbto-26&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="da9804f3ac9c8f69bdc0497bff3002e8af5a4a58" /></td>
<td class="checkbox"><input id="cbto-25" name="rev_to" onclick="if ($(&#x27;#cb-25&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-24&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="da9804f3ac9c8f69bdc0497bff3002e8af5a4a58" /></td>
<td class="committed_on">03/27/2015 09:15 pm</td>
<td class="author">vitriolix</td>
<td class="comments"><p>update content.py</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/ab699e85934f0142e2f73e9c4ae25134ed1d92ca" title="Revision ab699e85">ab699e85</a>
</td><td class="checkbox"><input id="cb-26" name="rev" onclick="$(&#x27;#cbto-27&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="ab699e85934f0142e2f73e9c4ae25134ed1d92ca" /></td>
<td class="checkbox"><input id="cbto-26" name="rev_to" onclick="if ($(&#x27;#cb-26&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-25&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="ab699e85934f0142e2f73e9c4ae25134ed1d92ca" /></td>
<td class="committed_on">03/25/2015 01:31 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>add persian and mena to content.py</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/b634a8e4cd1cb94c0a475c702ce4ee0237843e98" title="Revision b634a8e4">b634a8e4</a>
</td><td class="checkbox"><input id="cb-27" name="rev" onclick="$(&#x27;#cbto-28&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="b634a8e4cd1cb94c0a475c702ce4ee0237843e98" /></td>
<td class="checkbox"><input id="cbto-27" name="rev_to" onclick="if ($(&#x27;#cb-27&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-26&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="b634a8e4cd1cb94c0a475c702ce4ee0237843e98" /></td>
<td class="committed_on">03/12/2015 11:53 pm</td>
<td class="author">vitriolix</td>
<td class="comments"><p>add patch to content.py</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/b2ced8836a23492f8f92f007b034d7066bf7db76" title="Revision b2ced883">b2ced883</a>
</td><td class="checkbox"><input id="cb-28" name="rev" onclick="$(&#x27;#cbto-29&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="b2ced8836a23492f8f92f007b034d7066bf7db76" /></td>
<td class="checkbox"><input id="cbto-28" name="rev_to" onclick="if ($(&#x27;#cb-28&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-27&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="b2ced8836a23492f8f92f007b034d7066bf7db76" /></td>
<td class="committed_on">03/04/2015 11:18 pm</td>
<td class="author">vitriolix</td>
<td class="comments"><p>bump to 1035; update liger peg; update changelog</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/9278827bf663db61e20a3d16a8c5ddbf13071439" title="Revision 9278827b">9278827b</a>
</td><td class="checkbox"><input id="cb-29" name="rev" onclick="$(&#x27;#cbto-30&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="9278827bf663db61e20a3d16a8c5ddbf13071439" /></td>
<td class="checkbox"><input id="cbto-29" name="rev_to" onclick="if ($(&#x27;#cb-29&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-28&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="9278827bf663db61e20a3d16a8c5ddbf13071439" /></td>
<td class="committed_on">03/04/2015 10:53 pm</td>
<td class="author">vitriolix</td>
<td class="comments"><p>adding dressgate content pack to content.py; making obb push go to a temp file for prod pushes</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/7944f62468e1e84830c237de165ce9e59e095f39" title="Revision 7944f624">7944f624</a>
</td><td class="checkbox"><input id="cb-30" name="rev" onclick="$(&#x27;#cbto-31&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="7944f62468e1e84830c237de165ce9e59e095f39" /></td>
<td class="checkbox"><input id="cbto-30" name="rev_to" onclick="if ($(&#x27;#cb-30&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-29&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="7944f62468e1e84830c237de165ce9e59e095f39" /></td>
<td class="committed_on">02/28/2015 01:44 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>usability tweaks to content.py</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/8a751932afa29334e871ff67941681480bfa639b" title="Revision 8a751932">8a751932</a>
</td><td class="checkbox"><input id="cb-31" name="rev" onclick="$(&#x27;#cbto-32&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="8a751932afa29334e871ff67941681480bfa639b" /></td>
<td class="checkbox"><input id="cbto-31" name="rev_to" onclick="if ($(&#x27;#cb-31&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-30&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="8a751932afa29334e871ff67941681480bfa639b" /></td>
<td class="committed_on">02/26/2015 01:42 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>change to new content pack file naming convention</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/93eb1bb4ac30f9f4305252adc959675340540057" title="Revision 93eb1bb4">93eb1bb4</a>
</td><td class="checkbox"><input id="cb-32" name="rev" onclick="$(&#x27;#cbto-33&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="93eb1bb4ac30f9f4305252adc959675340540057" /></td>
<td class="checkbox"><input id="cbto-32" name="rev_to" onclick="if ($(&#x27;#cb-32&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-31&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="93eb1bb4ac30f9f4305252adc959675340540057" /></td>
<td class="committed_on">02/20/2015 06:51 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>update for new content pack system</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/029dcd73d366119ddd9303b2e1d08b040a09ba7f" title="Revision 029dcd73">029dcd73</a>
</td><td class="checkbox"><input id="cb-33" name="rev" onclick="$(&#x27;#cbto-34&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="029dcd73d366119ddd9303b2e1d08b040a09ba7f" /></td>
<td class="checkbox"><input id="cbto-33" name="rev_to" onclick="if ($(&#x27;#cb-33&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-32&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="029dcd73d366119ddd9303b2e1d08b040a09ba7f" /></td>
<td class="committed_on">02/19/2015 09:05 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>update content.py to add scp_push command and add new content pack zips to zip_content command</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/8730fcb09417cc95b7532a8dee6f0a9343d88a40" title="Revision 8730fcb0">8730fcb0</a>
</td><td class="checkbox"><input id="cb-34" name="rev" onclick="$(&#x27;#cbto-35&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="8730fcb09417cc95b7532a8dee6f0a9343d88a40" /></td>
<td class="checkbox"><input id="cbto-34" name="rev_to" onclick="if ($(&#x27;#cb-34&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-33&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="8730fcb09417cc95b7532a8dee6f0a9343d88a40" /></td>
<td class="committed_on">02/02/2015 03:21 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>fixing script name within content.py</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/337b20248dddc838dfe8193ebce857f8e7c6a950" title="Revision 337b2024">337b2024</a>
</td><td class="checkbox"><input id="cb-35" name="rev" onclick="$(&#x27;#cbto-36&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="337b20248dddc838dfe8193ebce857f8e7c6a950" /></td>
<td class="checkbox"><input id="cbto-35" name="rev_to" onclick="if ($(&#x27;#cb-35&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-34&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="337b20248dddc838dfe8193ebce857f8e7c6a950" /></td>
<td class="committed_on">11/13/2014 11:42 pm</td>
<td class="author">vitriolix</td>
<td class="comments"><p>fixing holoeverywhere build issue</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/a9c209a14f0d30e9ded0eade9130aa4922746c75" title="Revision a9c209a1">a9c209a1</a>
</td><td class="checkbox"><input id="cb-36" name="rev" onclick="$(&#x27;#cbto-37&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="a9c209a14f0d30e9ded0eade9130aa4922746c75" /></td>
<td class="checkbox"><input id="cbto-36" name="rev_to" onclick="if ($(&#x27;#cb-36&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-35&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="a9c209a14f0d30e9ded0eade9130aa4922746c75" /></td>
<td class="committed_on">11/13/2014 02:10 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>add some prints to scripts</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/3161748e3852977b5e20610faac58ce0e7d66935" title="Revision 3161748e">3161748e</a>
</td><td class="checkbox"><input id="cb-37" name="rev" onclick="$(&#x27;#cbto-38&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="3161748e3852977b5e20610faac58ce0e7d66935" /></td>
<td class="checkbox"><input id="cbto-37" name="rev_to" onclick="if ($(&#x27;#cb-37&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-36&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="3161748e3852977b5e20610faac58ce0e7d66935" /></td>
<td class="committed_on">11/07/2014 03:35 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>changed package name to org.storymaker.app from info.guardianproject.mrapp!  Death to Mr App!</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/fb5b9135f7472959a5fa0fcabc024c1acd65fbf4" title="Revision fb5b9135">fb5b9135</a>
</td><td class="checkbox"><input id="cb-38" name="rev" onclick="$(&#x27;#cbto-39&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="fb5b9135f7472959a5fa0fcabc024c1acd65fbf4" /></td>
<td class="checkbox"><input id="cbto-38" name="rev_to" onclick="if ($(&#x27;#cb-38&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-37&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="fb5b9135f7472959a5fa0fcabc024c1acd65fbf4" /></td>
<td class="committed_on">11/05/2014 11:57 pm</td>
<td class="author">vitriolix</td>
<td class="comments"><p>update content.py to put .obb file in /sdcard/Android/data/&lt;packagename&gt;/files/</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/00295b0f6bb14491c8f70b5a3d55c368dda2d37a" title="Revision 00295b0f">00295b0f</a>
</td><td class="checkbox"><input id="cb-39" name="rev" onclick="$(&#x27;#cbto-40&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="00295b0f6bb14491c8f70b5a3d55c368dda2d37a" /></td>
<td class="checkbox"><input id="cbto-39" name="rev_to" onclick="if ($(&#x27;#cb-39&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-38&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="00295b0f6bb14491c8f70b5a3d55c368dda2d37a" /></td>
<td class="committed_on">11/03/2014 05:27 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>fix issues with content script</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/edf5cb6a860f3a3d5a22d99db55feb29db30b0fb" title="Revision edf5cb6a">edf5cb6a</a>
</td><td class="checkbox"><input id="cb-40" name="rev" onclick="$(&#x27;#cbto-41&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="edf5cb6a860f3a3d5a22d99db55feb29db30b0fb" /></td>
<td class="checkbox"><input id="cbto-40" name="rev_to" onclick="if ($(&#x27;#cb-40&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-39&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="edf5cb6a860f3a3d5a22d99db55feb29db30b0fb" /></td>
<td class="committed_on">11/03/2014 05:01 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>updating content script</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/wrapp/repository/revisions/26f30fc2b3166cccae9ddd0f48e4c606a9d98d7e" title="Revision 26f30fc2">26f30fc2</a>
</td><td class="checkbox"></td>
<td class="checkbox"><input id="cbto-41" name="rev_to" onclick="if ($(&#x27;#cb-41&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-40&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="26f30fc2b3166cccae9ddd0f48e4c606a9d98d7e" /></td>
<td class="committed_on">11/03/2014 04:35 am</td>
<td class="author">vitriolix</td>
<td class="comments"><p>adding content.py script to automate content bundling and translation processing</p></td>
</tr>
</tbody>
</table>
<input type="submit" value="View differences" />
</form>



        
        <div style="clear:both;"></div>
    </div>
</div>
</div>

<div id="ajax-indicator" style="display:none;"><span>Loading...</span></div>
<div id="ajax-modal" style="display:none;"></div>

<div id="footer">
  <div class="bgl"><div class="bgr">
    Powered by <a href="http://www.redmine.org/">Redmine</a> &copy; 2006-2013 Jean-Philippe Lang
  </div></div>
</div>
</div>
</div>

</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>/jenkins-build.sh - Changes - GnuPrivacyGuard - Guardian Project Dev (ARCHIVED SITE)</title>
<meta name="description" content="Redmine" />
<meta name="keywords" content="issue,bug,tracker" />
<meta content="authenticity_token" name="csrf-param" />
<meta content="TFkt2Jobv7aDVO+oYsmsHbD4ygyDFiolu7KeLK56zU8=" name="csrf-token" />
<link rel='shortcut icon' href='../../../../favicon.ico%3F1371683577' />
<link href="../../../../stylesheets/jquery/jquery-ui-1.9.2.css%3F1371683503.css" media="all" rel="stylesheet" type="text/css" />
<link href="../../../../themes/a1/stylesheets/application.css%3F1386177785.css" media="all" rel="stylesheet" type="text/css" />

<script src="../../../../javascripts/jquery-1.8.3-ui-1.9.2-ujs-2.0.3.js%3F1371683503" type="text/javascript"></script>
<script src="../../../../javascripts/application.js%3F1371683577" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
$(window).load(function(){ warnLeavingUnsaved('The current page contains unsaved text that will be lost if you leave this page.'); });
//]]>
</script>


<!-- page specific tags -->
  <script src="../../../../javascripts/repository_navigation.js%3F1371683503" type="text/javascript"></script>
<link href="../../../../stylesheets/scm.css%3F1371683577.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body class="theme-A1 controller-repositories action-changes">
<div id="wrapper">
<div id="wrapper2">
<div id="wrapper3">
<div id="top-menu">
    <div id="account">
        <ul><li><a href="../../../../login.html" class="login">Sign in</a></li>
<li><a href="../../../../account/register.html" class="register">Register</a></li></ul>    </div>
    
    <ul><li><a href="../../../../index.html" class="home">Home</a></li>
<li><a href="../../../../projects.html" class="projects">Projects</a></li>
<li><a href="http://www.redmine.org/guide" class="help">Help</a></li></ul></div>

<div id="header">
    <div id="quick-search">
        <form accept-charset="UTF-8" action="../../search.html" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
        <input name="changesets" type="hidden" value="1" />
        <label for='q'>
          <a href="../../search.html" accesskey="4">Search</a>:
        </label>
        <input accesskey="f" class="small" id="q" name="q" size="20" type="text" />
</form>        
    </div>

    <h1><a href="../../../psst%3Fjump=repository.html" class="root">PSST</a> » GnuPrivacyGuard</h1>

    <div id="main-menu">
        <ul><li><a href="../../../gpgandroid.html" class="overview">Overview</a></li>
<li><a href="../../activity.html" class="activity">Activity</a></li>
<li><a href="../../wiki.html" class="wiki">Wiki</a></li>
<li><a href="../../repository.html" class="repository selected">Repository</a></li></ul>
    </div>
</div>

<div id="main" class="nosidebar">
    <div id="sidebar">
        
        
    </div>

    <div id="content">
        
        

<div class="contextual">
  
<a href="../statistics.html" class="icon icon-stats">Statistics</a>

<form accept-charset="UTF-8" action="https://dev.guardianproject.info/projects/gpgandroid/repository/changes/jenkins-build.sh" id="revision_selector" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>  <!-- Branches Dropdown -->
    | Branch: 
    <select id="branch" name="branch"><option value=""></option>
<option value="master" selected="selected">master</option>
<option value="ui">ui</option></select>

    | Tag: 
    <select id="tag" name="tag"><option value=""></option>
<option value="0.0">0.0</option>
<option value="0.2">0.2</option>
<option value="0.2.1">0.2.1</option>
<option value="0.3">0.3</option>
<option value="0.3.1">0.3.1</option>
<option value="0.3.2">0.3.2</option>
<option value="v0.0">v0.0</option></select>

    | Revision: 
    <input id="rev" name="rev" size="8" type="text" value="master" />
</form>
</div>

<h2><a href="../revisions/master/show.html">gnupg-for-android</a>
    / <a href="jenkins-build.sh%3Frev=master.html">jenkins-build.sh</a>
@ master

</h2>


<p>
History |
    <a href="https://dev.guardianproject.info/projects/gpgandroid/repository/revisions/master/entry/jenkins-build.sh">View</a> |
    <a href="https://dev.guardianproject.info/projects/gpgandroid/repository/revisions/master/annotate/jenkins-build.sh">Annotate</a> |
<a href="https://dev.guardianproject.info/projects/gpgandroid/repository/revisions/master/raw/jenkins-build.sh">Download</a>
(790 Bytes)
</p>






<form accept-charset="UTF-8" action="https://dev.guardianproject.info/projects/gpgandroid/repository/diff/jenkins-build.sh" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
<table class="list changesets">
<thead><tr>
<th>#</th>
<th></th>
<th></th>
<th>Date</th>
<th>Author</th>
<th>Comment</th>
</tr></thead>
<tbody>
<tr class="changeset odd">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/gpgandroid/repository/revisions/981c8de46a9d2ea3c32f1f3147202ed06323251c" title="Revision 981c8de4">981c8de4</a>
</td><td class="checkbox"><input checked="checked" id="cb-1" name="rev" onclick="$(&#x27;#cbto-2&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="981c8de46a9d2ea3c32f1f3147202ed06323251c" /></td>
<td class="checkbox"></td>
<td class="committed_on">05/19/2014 04:26 pm</td>
<td class="author">hans</td>
<td class="comments"><p>jenkins-build: only feed .c and .h files to cppcheck</p>


	<p>Before, cppcheck was trying to check Makefiles and *.patch, so stop that ;)</p>


	<p>Sort the file list since cppcheck checks the files in the order that it<br />receives them.</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/gpgandroid/repository/revisions/fe047918cd20cea729dc3fc6474272031560fab2" title="Revision fe047918">fe047918</a>
</td><td class="checkbox"><input id="cb-2" name="rev" onclick="$(&#x27;#cbto-3&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="fe047918cd20cea729dc3fc6474272031560fab2" /></td>
<td class="checkbox"><input checked="checked" id="cbto-2" name="rev_to" onclick="if ($(&#x27;#cb-2&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-1&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="fe047918cd20cea729dc3fc6474272031560fab2" /></td>
<td class="committed_on">05/19/2014 04:26 pm</td>
<td class="author">hans</td>
<td class="comments"><p>quadrupling the amount of #ifdef variations cppcheck will try</p>


	<p>GnuPG libs have a lot of #ifdefs, so we need a lot more than the default of<br />twelve.  This will probably also double the amount of time cppcheck takes<br />to run.</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/gpgandroid/repository/revisions/1ed7b155e64a63870accc3bddf6817fff3d78f1c" title="Revision 1ed7b155">1ed7b155</a>
</td><td class="checkbox"><input id="cb-3" name="rev" onclick="$(&#x27;#cbto-4&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="1ed7b155e64a63870accc3bddf6817fff3d78f1c" /></td>
<td class="checkbox"><input id="cbto-3" name="rev_to" onclick="if ($(&#x27;#cb-3&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-2&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="1ed7b155e64a63870accc3bddf6817fff3d78f1c" /></td>
<td class="committed_on">03/13/2014 08:50 pm</td>
<td class="author">hans</td>
<td class="comments"><p>jenkins-build.sh: set the version code and name based on current date</p>


	<p>This will differentiate the debug builds from the normal builds. The debug<br />builds will never be upgraded by the release builds since the debug builds<br />will always have a much larger versionCode.  Also, the date it was built...</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/gpgandroid/repository/revisions/cb475897a887a566d0186a359de5efe8a2fcad9c" title="Revision cb475897">cb475897</a>
</td><td class="checkbox"></td>
<td class="checkbox"><input id="cbto-4" name="rev_to" onclick="if ($(&#x27;#cb-4&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-3&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="cb475897a887a566d0186a359de5efe8a2fcad9c" /></td>
<td class="committed_on">01/30/2014 07:15 pm</td>
<td class="author">hans</td>
<td class="comments"><p>commit jenkins build script to git</p>


	<p>This is getting too elaborate to manage in the little Jenkins text form<br />field...</p>


	<p>refs #1270</p></td>
</tr>
</tbody>
</table>
<input type="submit" value="View differences" />
</form>



        
        <div style="clear:both;"></div>
    </div>
</div>
</div>

<div id="ajax-indicator" style="display:none;"><span>Loading...</span></div>
<div id="ajax-modal" style="display:none;"></div>

<div id="footer">
  <div class="bgl"><div class="bgr">
    Powered by <a href="http://www.redmine.org/">Redmine</a> &copy; 2006-2013 Jean-Philippe Lang
  </div></div>
</div>
</div>
</div>

</body>
</html>

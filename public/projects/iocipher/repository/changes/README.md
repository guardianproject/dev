<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>/README.md - Changes - IOCipher - Guardian Project Dev (ARCHIVED SITE)</title>
<meta name="description" content="Redmine" />
<meta name="keywords" content="issue,bug,tracker" />
<meta content="authenticity_token" name="csrf-param" />
<meta content="TFkt2Jobv7aDVO+oYsmsHbD4ygyDFiolu7KeLK56zU8=" name="csrf-token" />
<link rel='shortcut icon' href='/favicon.ico?1371683577' />
<link href="/stylesheets/jquery/jquery-ui-1.9.2.css?1371683503" media="all" rel="stylesheet" type="text/css" />
<link href="/themes/a1/stylesheets/application.css?1386177785" media="all" rel="stylesheet" type="text/css" />

<script src="/javascripts/jquery-1.8.3-ui-1.9.2-ujs-2.0.3.js?1371683503" type="text/javascript"></script>
<script src="/javascripts/application.js?1371683577" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
$(window).load(function(){ warnLeavingUnsaved('The current page contains unsaved text that will be lost if you leave this page.'); });
//]]>
</script>


<!-- page specific tags -->
  <script src="/javascripts/repository_navigation.js?1371683503" type="text/javascript"></script>
<link href="/stylesheets/scm.css?1371683577" media="screen" rel="stylesheet" type="text/css" />
</head>
<body class="theme-A1 controller-repositories action-changes">
<div id="wrapper">
<div id="wrapper2">
<div id="wrapper3">
<div id="top-menu">
    <div id="account">
        <ul><li><a href="/login" class="login">Sign in</a></li>
<li><a href="/account/register" class="register">Register</a></li></ul>    </div>
    
    <ul><li><a href="/" class="home">Home</a></li>
<li><a href="/projects" class="projects">Projects</a></li>
<li><a href="http://www.redmine.org/guide" class="help">Help</a></li></ul></div>

<div id="header">
    <div id="quick-search">
        <form accept-charset="UTF-8" action="/projects/iocipher/search" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
        <input name="changesets" type="hidden" value="1" />
        <label for='q'>
          <a href="/projects/iocipher/search" accesskey="4">Search</a>:
        </label>
        <input accesskey="f" class="small" id="q" name="q" size="20" type="text" />
</form>        
    </div>

    <h1><a href="/projects/libs?jump=repository" class="root">Developer Libraries</a> » IOCipher</h1>

    <div id="main-menu">
        <ul><li><a href="/projects/iocipher" class="overview">Overview</a></li>
<li><a href="/projects/iocipher/activity" class="activity">Activity</a></li>
<li><a href="/projects/iocipher/roadmap" class="roadmap">Roadmap</a></li>
<li><a href="/projects/iocipher/issues" class="issues">Issues</a></li>
<li><a href="/projects/iocipher/issues/new" accesskey="7" class="new-issue">New issue</a></li>
<li><a href="/projects/iocipher/issues/gantt" class="gantt">Gantt</a></li>
<li><a href="/projects/iocipher/issues/calendar" class="calendar">Calendar</a></li>
<li><a href="/projects/iocipher/news" class="news">News</a></li>
<li><a href="/projects/iocipher/wiki" class="wiki">Wiki</a></li>
<li><a href="/projects/iocipher/repository" class="repository selected">Repository</a></li></ul>
    </div>
</div>

<div id="main" class="nosidebar">
    <div id="sidebar">
        
        
    </div>

    <div id="content">
        
        

<div class="contextual">
  
<a href="/projects/iocipher/repository/statistics" class="icon icon-stats">Statistics</a>

<form accept-charset="UTF-8" action="/projects/iocipher/repository/changes/README.md" id="revision_selector" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>  <!-- Branches Dropdown -->
    | Branch: 
    <select id="branch" name="branch"><option value=""></option>
<option value="master" selected="selected">master</option></select>

    | Tag: 
    <select id="tag" name="tag"><option value=""></option>
<option value="0.4">0.4</option>
<option value="v0.1">v0.1</option>
<option value="v0.2">v0.2</option>
<option value="v0.3">v0.3</option></select>

    | Revision: 
    <input id="rev" name="rev" size="8" type="text" value="master" />
</form>
</div>

<h2><a href="/projects/iocipher/repository/revisions/master/show">iocipher</a>
    / <a href="/projects/iocipher/repository/changes/README.md?rev=master">README.md</a>
@ master

</h2>


<p>
History |
    <a href="/projects/iocipher/repository/revisions/master/entry/README.md">View</a> |
    <a href="/projects/iocipher/repository/revisions/master/annotate/README.md">Annotate</a> |
<a href="/projects/iocipher/repository/revisions/master/raw/README.md">Download</a>
(5.13 KB)
</p>






<form accept-charset="UTF-8" action="/projects/iocipher/repository/diff/README.md" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
<table class="list changesets">
<thead><tr>
<th>#</th>
<th></th>
<th></th>
<th>Date</th>
<th>Author</th>
<th>Comment</th>
</tr></thead>
<tbody>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/iocipher/repository/revisions/3ea7e858a913c453665a1cd97a5133ec3b7b4cf3" title="Revision 3ea7e858">3ea7e858</a>
</td><td class="checkbox"><input checked="checked" id="cb-1" name="rev" onclick="$(&#x27;#cbto-2&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="3ea7e858a913c453665a1cd97a5133ec3b7b4cf3" /></td>
<td class="checkbox"></td>
<td class="committed_on">01/18/2017 10:29 am</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>update build instructions in README</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/iocipher/repository/revisions/028f64faeb6333a003486183508a333a61e0f0ce" title="Revision 028f64fa">028f64fa</a>
</td><td class="checkbox"><input id="cb-2" name="rev" onclick="$(&#x27;#cbto-3&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="028f64faeb6333a003486183508a333a61e0f0ce" /></td>
<td class="checkbox"><input checked="checked" id="cbto-2" name="rev_to" onclick="if ($(&#x27;#cb-2&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-1&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="028f64faeb6333a003486183508a333a61e0f0ce" /></td>
<td class="committed_on">01/12/2017 01:35 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>update versions in README</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/iocipher/repository/revisions/368bafe2335077380e2fc45a39ddd2b5a791f0de" title="Revision 368bafe2">368bafe2</a>
</td><td class="checkbox"><input id="cb-3" name="rev" onclick="$(&#x27;#cbto-4&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="368bafe2335077380e2fc45a39ddd2b5a791f0de" /></td>
<td class="checkbox"><input id="cbto-3" name="rev_to" onclick="if ($(&#x27;#cb-3&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-2&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="368bafe2335077380e2fc45a39ddd2b5a791f0de" /></td>
<td class="committed_on">09/18/2015 01:46 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>generate to AARs: complete "standalone" and only IOCipher</p>


	<p>If an app is using SQLCipher-for-Android already, then that app just needs<br />the IOCipher libs.  If the app is not using SQLCipher at all, then the<br />standalone version includes the required SQLCipher shared libraries.</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="/projects/iocipher/repository/revisions/454352b490785bcc9affb51bf865bffed3068962" title="Revision 454352b4">454352b4</a>
</td><td class="checkbox"><input id="cb-4" name="rev" onclick="$(&#x27;#cbto-5&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="454352b490785bcc9affb51bf865bffed3068962" /></td>
<td class="checkbox"><input id="cbto-4" name="rev_to" onclick="if ($(&#x27;#cb-4&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-3&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="454352b490785bcc9affb51bf865bffed3068962" /></td>
<td class="committed_on">09/18/2015 01:38 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>include phrase about LGPL compatibility with Java in README</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="/projects/iocipher/repository/revisions/1fb00b4c246134a41dee3a24b0c95b41f9d98339" title="Revision 1fb00b4c">1fb00b4c</a>
</td><td class="checkbox"></td>
<td class="checkbox"><input id="cbto-5" name="rev_to" onclick="if ($(&#x27;#cb-5&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-4&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="1fb00b4c246134a41dee3a24b0c95b41f9d98339" /></td>
<td class="committed_on">02/17/2015 02:32 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>convert README to markdown</p></td>
</tr>
</tbody>
</table>
<input type="submit" value="View differences" />
</form>



        
        <div style="clear:both;"></div>
    </div>
</div>
</div>

<div id="ajax-indicator" style="display:none;"><span>Loading...</span></div>
<div id="ajax-modal" style="display:none;"></div>

<div id="footer">
  <div class="bgl"><div class="bgr">
    Powered by <a href="http://www.redmine.org/">Redmine</a> &copy; 2006-2013 Jean-Philippe Lang
  </div></div>
</div>
</div>
</div>

</body>
</html>

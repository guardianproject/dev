<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>/README.md - Changes - KeySync - Guardian Project Dev (ARCHIVED SITE)</title>
<meta name="description" content="Redmine" />
<meta name="keywords" content="issue,bug,tracker" />
<meta content="authenticity_token" name="csrf-param" />
<meta content="TFkt2Jobv7aDVO+oYsmsHbD4ygyDFiolu7KeLK56zU8=" name="csrf-token" />
<link rel='shortcut icon' href='../../../../favicon.ico%3F1371683577' />
<link href="../../../../stylesheets/jquery/jquery-ui-1.9.2.css%3F1371683503.css" media="all" rel="stylesheet" type="text/css" />
<link href="../../../../themes/a1/stylesheets/application.css%3F1386177785.css" media="all" rel="stylesheet" type="text/css" />

<script src="../../../../javascripts/jquery-1.8.3-ui-1.9.2-ujs-2.0.3.js%3F1371683503" type="text/javascript"></script>
<script src="../../../../javascripts/application.js%3F1371683577" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
$(window).load(function(){ warnLeavingUnsaved('The current page contains unsaved text that will be lost if you leave this page.'); });
//]]>
</script>


<!-- page specific tags -->
  <script src="../../../../javascripts/repository_navigation.js%3F1371683503" type="text/javascript"></script>
<link href="../../../../stylesheets/scm.css%3F1371683577.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body class="theme-A1 controller-repositories action-changes">
<div id="wrapper">
<div id="wrapper2">
<div id="wrapper3">
<div id="top-menu">
    <div id="account">
        <ul><li><a href="../../../../login.html" class="login">Sign in</a></li>
<li><a href="../../../../account/register.html" class="register">Register</a></li></ul>    </div>
    
    <ul><li><a href="../../../../index.html" class="home">Home</a></li>
<li><a href="../../../../projects.html" class="projects">Projects</a></li>
<li><a href="http://www.redmine.org/guide" class="help">Help</a></li></ul></div>

<div id="header">
    <div id="quick-search">
        <form accept-charset="UTF-8" action="../../search.html" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
        <input name="changesets" type="hidden" value="1" />
        <label for='q'>
          <a href="../../search.html" accesskey="4">Search</a>:
        </label>
        <input accesskey="f" class="small" id="q" name="q" size="20" type="text" />
</form>        
    </div>

    <h1><a href="../../../core%3Fjump=repository.html" class="root">Core Apps</a> » KeySync</h1>

    <div id="main-menu">
        <ul><li><a href="../../../keysync.html" class="overview">Overview</a></li>
<li><a href="../../activity.html" class="activity">Activity</a></li>
<li><a href="../../roadmap.html" class="roadmap">Roadmap</a></li>
<li><a href="../../issues.html" class="issues">Issues</a></li>
<li><a href="../../issues/new.html" accesskey="7" class="new-issue">New issue</a></li>
<li><a href="../../wiki.html" class="wiki">Wiki</a></li>
<li><a href="../../repository.html" class="repository selected">Repository</a></li></ul>
    </div>
</div>

<div id="main" class="nosidebar">
    <div id="sidebar">
        
        
    </div>

    <div id="content">
        
        

<div class="contextual">
  
<a href="../statistics.html" class="icon icon-stats">Statistics</a>

<form accept-charset="UTF-8" action="https://dev.guardianproject.info/projects/keysync/repository/changes/README.md" id="revision_selector" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>  <!-- Branches Dropdown -->
    | Branch: 
    <select id="branch" name="branch"><option value=""></option>
<option value="master" selected="selected">master</option></select>

    | Tag: 
    <select id="tag" name="tag"><option value=""></option>
<option value="0.0.0">0.0.0</option>
<option value="0.0.1">0.0.1</option>
<option value="0.0.2">0.0.2</option>
<option value="0.1">0.1</option>
<option value="0.2">0.2</option>
<option value="0.2.0-beta2">0.2.0-beta2</option>
<option value="0.2.0-beta3">0.2.0-beta3</option>
<option value="0.2.1">0.2.1</option>
<option value="0.2.1.1">0.2.1.1</option>
<option value="0.2.2">0.2.2</option></select>

    | Revision: 
    <input id="rev" name="rev" size="8" type="text" value="master" />
</form>
</div>

<h2><a href="../revisions/master/show.html">keysync</a>
    / <a href="README.md%3Frev=master.html">README.md</a>
@ master

</h2>


<p>
History |
    <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/master/entry/README.md">View</a> |
    <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/master/annotate/README.md">Annotate</a> |
<a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/master/raw/README.md">Download</a>
(14 KB)
</p>






<form accept-charset="UTF-8" action="https://dev.guardianproject.info/projects/keysync/repository/diff/README.md" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
<table class="list changesets">
<thead><tr>
<th>#</th>
<th></th>
<th></th>
<th>Date</th>
<th>Author</th>
<th>Comment</th>
</tr></thead>
<tbody>
<tr class="changeset odd">
<td class="id">
  <a href="../revisions/079aac417fd02188c0dca5c0221f741c5fcb3a0c.html" title="Revision 079aac41">079aac41</a>
</td><td class="checkbox"><input checked="checked" id="cb-1" name="rev" onclick="$(&#x27;#cbto-2&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="079aac417fd02188c0dca5c0221f741c5fcb3a0c" /></td>
<td class="checkbox"></td>
<td class="committed_on">09/17/2015 01:45 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>add links to the README</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/7a912ea5b5911c19c6f33e82233a0314e131ffcd" title="Revision 7a912ea5">7a912ea5</a>
</td><td class="checkbox"><input id="cb-2" name="rev" onclick="$(&#x27;#cbto-3&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="7a912ea5b5911c19c6f33e82233a0314e131ffcd" /></td>
<td class="checkbox"><input checked="checked" id="cbto-2" name="rev_to" onclick="if ($(&#x27;#cb-2&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-1&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="7a912ea5b5911c19c6f33e82233a0314e131ffcd" /></td>
<td class="committed_on">06/24/2014 04:54 pm</td>
<td class="author">Timothy Redaelli </td>
<td class="comments"><p>switch from BeautifulSoup 3 to 4 (closes <a href="https://dev.guardianproject.info/issues/24" class="issue tracker-1 status-5 priority-4 priority-default closed" title="OTR initiated on chat open even if not requested in settings (Closed)">#24</a>)</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/b077837e8a2dccedefe9965a27809610123f4db5" title="Revision b077837e">b077837e</a>
</td><td class="checkbox"><input id="cb-3" name="rev" onclick="$(&#x27;#cbto-4&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="b077837e8a2dccedefe9965a27809610123f4db5" /></td>
<td class="checkbox"><input id="cbto-3" name="rev_to" onclick="if ($(&#x27;#cb-3&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-2&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="b077837e8a2dccedefe9965a27809610123f4db5" /></td>
<td class="committed_on">03/18/2014 05:29 pm</td>
<td class="author">Abel Luck </td>
<td class="comments"><p>gajim: implement full account reading</p>


	<p>Exporting public and private keys from Gajim now works.</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/fe6419cccec931651cdbb853d9f4641d82296f25" title="Revision fe6419cc">fe6419cc</a>
</td><td class="checkbox"><input id="cb-4" name="rev" onclick="$(&#x27;#cbto-5&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="fe6419cccec931651cdbb853d9f4641d82296f25" /></td>
<td class="checkbox"><input id="cbto-4" name="rev_to" onclick="if ($(&#x27;#cb-4&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-3&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="fe6419cccec931651cdbb853d9f4641d82296f25" /></td>
<td class="committed_on">03/11/2014 04:13 pm</td>
<td class="author">Abel Luck </td>
<td class="comments"><p>readme: Add missing build deps for Fedora 20</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/d2dd9c20ad633f626a858fa936a518404d33d7e0" title="Revision d2dd9c20">d2dd9c20</a>
</td><td class="checkbox"><input id="cb-5" name="rev" onclick="$(&#x27;#cbto-6&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="d2dd9c20ad633f626a858fa936a518404d33d7e0" /></td>
<td class="checkbox"><input id="cbto-5" name="rev_to" onclick="if ($(&#x27;#cb-5&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-4&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="d2dd9c20ad633f626a858fa936a518404d33d7e0" /></td>
<td class="committed_on">10/16/2013 12:40 am</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>python-potr has been released, so special instructions are no longer needed</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/2eeaa6150647d657d66a6e90313011ce501325d0" title="Revision 2eeaa615">2eeaa615</a>
</td><td class="checkbox"><input id="cb-6" name="rev" onclick="$(&#x27;#cbto-7&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="2eeaa6150647d657d66a6e90313011ce501325d0" /></td>
<td class="checkbox"><input id="cbto-6" name="rev_to" onclick="if ($(&#x27;#cb-6&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-5&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="2eeaa6150647d657d66a6e90313011ce501325d0" /></td>
<td class="committed_on">10/10/2013 10:53 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>added note that keysync does not address OTR issues with multi-party (closes <a href="https://dev.guardianproject.info/issues/20" class="issue tracker-2 status-5 priority-4 priority-default closed" title="Collect data about nearby devices such as other phones, wireless access points and cell towers (Closed)">#20</a>)</p>


	<p>Added some language to further clarify what syncing OTR identity and trust<br />relationships means, and added language to clarify what KeySync is <strong>not</strong><br />meant to do.</p>


	<p>thanks to the.solipsist for contributing to this.</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/a08a892e712399f8f884ff3ad8c7ef2f4aad85c4" title="Revision a08a892e">a08a892e</a>
</td><td class="checkbox"><input id="cb-7" name="rev" onclick="$(&#x27;#cbto-8&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="a08a892e712399f8f884ff3ad8c7ef2f4aad85c4" /></td>
<td class="checkbox"><input id="cbto-7" name="rev_to" onclick="if ($(&#x27;#cb-7&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-6&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="a08a892e712399f8f884ff3ad8c7ef2f4aad85c4" /></td>
<td class="committed_on">10/10/2013 12:26 pm</td>
<td class="author">Abel Luck </td>
<td class="comments"><p>readme: reference windows build instructions</p>


	<p>also change ~/ -> <span>APPDATA</span> for pidgin on windows</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/100f78d7e12cd77c6c5027033189eb0f18efd811" title="Revision 100f78d7">100f78d7</a>
</td><td class="checkbox"><input id="cb-8" name="rev" onclick="$(&#x27;#cbto-9&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="100f78d7e12cd77c6c5027033189eb0f18efd811" /></td>
<td class="checkbox"><input id="cbto-8" name="rev_to" onclick="if ($(&#x27;#cb-8&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-7&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="100f78d7e12cd77c6c5027033189eb0f18efd811" /></td>
<td class="committed_on">10/08/2013 06:50 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>implement MTP detection and syncing for Mac OS X, which has no MTP support</p>


	<p>pymtp/libmtp provide the MTP support, we also have to think about Google's<br />Android File Transfer app, since its also a libmtp app, and it will<br />conflict with KeySync.</p>


	<p>refs <a href="../../../../issues/1939.html" class="issue tracker-3 status-5 priority-4 priority-default closed" title="automatically detect and sync to ChatSecure (Closed)">#1939</a>, <a href="../../../../issues/1967.html" class="issue tracker-3 status-5 priority-4 priority-default closed" title="implement preliminary multi-source GUI (Closed)">#1967</a>, <a href="../../../../issues/1968.html" class="issue tracker-3 status-1 priority-4 priority-default" title="implement GUI for multi-app two-way sync (New)">#1968</a>, <a href="../../../../issues/1870.html" class="issue tracker-3 status-5 priority-4 priority-default closed" title="create Mac OS X KeySync.app (Closed)">#1870</a></p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/3f4999a46e9cd25fe9c7d5e6a15ffc3085e2ac51" title="Revision 3f4999a4">3f4999a4</a>
</td><td class="checkbox"><input id="cb-9" name="rev" onclick="$(&#x27;#cbto-10&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="3f4999a46e9cd25fe9c7d5e6a15ffc3085e2ac51" /></td>
<td class="checkbox"><input id="cbto-9" name="rev_to" onclick="if ($(&#x27;#cb-9&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-8&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="3f4999a46e9cd25fe9c7d5e6a15ffc3085e2ac51" /></td>
<td class="committed_on">10/08/2013 03:25 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>document dependencies in README and setup.py</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/631e7fa1c64368c7cb44b43a7dc4d6aba1573403" title="Revision 631e7fa1">631e7fa1</a>
</td><td class="checkbox"><input id="cb-10" name="rev" onclick="$(&#x27;#cbto-11&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="631e7fa1c64368c7cb44b43a7dc4d6aba1573403" /></td>
<td class="checkbox"><input id="cbto-10" name="rev_to" onclick="if ($(&#x27;#cb-10&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-9&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="631e7fa1c64368c7cb44b43a7dc4d6aba1573403" /></td>
<td class="committed_on">10/07/2013 07:54 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>update Mac OS X build instructions</p>


	<p>refs <a href="../../../../issues/1870.html" class="issue tracker-3 status-5 priority-4 priority-default closed" title="create Mac OS X KeySync.app (Closed)">#1870</a></p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="../revisions/4f30872571ee252202b00d3436b8204c745c390b.html" title="Revision 4f308725">4f308725</a>
</td><td class="checkbox"><input id="cb-11" name="rev" onclick="$(&#x27;#cbto-12&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="4f30872571ee252202b00d3436b8204c745c390b" /></td>
<td class="checkbox"><input id="cbto-11" name="rev_to" onclick="if ($(&#x27;#cb-11&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-10&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="4f30872571ee252202b00d3436b8204c745c390b" /></td>
<td class="committed_on">09/20/2013 09:54 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>create pip+virtual+py2app build environment for making a Mac OS X .app</p>


	<p>This also lays the groundwork for an easily repeatable py2exe setup.</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/bce269636b3ef25c7a62c0e3596a3c3e3d802197" title="Revision bce26963">bce26963</a>
</td><td class="checkbox"><input id="cb-12" name="rev" onclick="$(&#x27;#cbto-13&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="bce269636b3ef25c7a62c0e3596a3c3e3d802197" /></td>
<td class="checkbox"><input id="cbto-12" name="rev_to" onclick="if ($(&#x27;#cb-12&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-11&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="bce269636b3ef25c7a62c0e3596a3c3e3d802197" /></td>
<td class="committed_on">09/20/2013 09:54 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>switch to setuptools and add dependencies to automate them elsewhere</p>


	<p>Using setuptools means setup.py can have install_requires, and that is then<br />used as the canonical list of dependencies for pip and the debian package.</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/322b22b883b52703ef37fe8c771cb691567dfc32" title="Revision 322b22b8">322b22b8</a>
</td><td class="checkbox"><input id="cb-13" name="rev" onclick="$(&#x27;#cbto-14&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="322b22b883b52703ef37fe8c771cb691567dfc32" /></td>
<td class="checkbox"><input id="cbto-13" name="rev_to" onclick="if ($(&#x27;#cb-13&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-12&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="322b22b883b52703ef37fe8c771cb691567dfc32" /></td>
<td class="committed_on">09/20/2013 09:54 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>rename Gibberbot to ChatSecure everywhere in the code and documentation</p>


	<p>Its official: Gibberbot is now ChatSecure for Android!</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/2b5ef99469a4f33bde8797bb863af670d313f5d1" title="Revision 2b5ef994">2b5ef994</a>
</td><td class="checkbox"><input id="cb-14" name="rev" onclick="$(&#x27;#cbto-15&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="2b5ef99469a4f33bde8797bb863af670d313f5d1" /></td>
<td class="checkbox"><input id="cbto-14" name="rev_to" onclick="if ($(&#x27;#cb-14&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-13&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="2b5ef99469a4f33bde8797bb863af670d313f5d1" /></td>
<td class="committed_on">09/20/2013 09:54 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>detect which supported apps might be currently running</p>


	<p>If an IM app is running, then KeySync cannot safely write new OTR files for<br />that app.  This check will be used to detect what apps are running, and<br />prompt the user to quit them if they want to sync with those apps.</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/d7bd52bb53093764bb2aedcfb8a27fdb9e0bf0bb" title="Revision d7bd52bb">d7bd52bb</a>
</td><td class="checkbox"><input id="cb-15" name="rev" onclick="$(&#x27;#cbto-16&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="d7bd52bb53093764bb2aedcfb8a27fdb9e0bf0bb" /></td>
<td class="checkbox"><input id="cbto-15" name="rev_to" onclick="if ($(&#x27;#cb-15&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-14&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="d7bd52bb53093764bb2aedcfb8a27fdb9e0bf0bb" /></td>
<td class="committed_on">09/16/2013 10:01 pm</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>jitsi: carefully target specific service cases when writing out (closes <a href="../../../../issues/19.html" class="issue tracker-2 status-5 priority-4 priority-default closed" title="Collect data from existing smartphone sensors (Closed)">#19</a>)</p>


	<p>The existing patterns were too broad, for example, this code:<br />  elif 'gmail' in key['name'].split('@')[1]<br />would also trigger a match if the domain name was "enigmail.com".</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/73730aec981f0ce0f566696eb934a8a5a0f79646" title="Revision 73730aec">73730aec</a>
</td><td class="checkbox"><input id="cb-16" name="rev" onclick="$(&#x27;#cbto-17&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="73730aec981f0ce0f566696eb934a8a5a0f79646" /></td>
<td class="checkbox"><input id="cbto-16" name="rev_to" onclick="if ($(&#x27;#cb-16&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-15&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="73730aec981f0ce0f566696eb934a8a5a0f79646" /></td>
<td class="committed_on">09/16/2013 06:09 pm</td>
<td class="author">duy </td>
<td class="comments"><p>added support for xchat (closes <a href="../../../../issues/17.html" class="issue tracker-2 status-3 priority-4 priority-default" title="add temperature sensor support (Resolved)">#17</a>)</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/8e69a29547299d7eeeab4f86b21cb8db9801d753" title="Revision 8e69a295">8e69a295</a>
</td><td class="checkbox"><input id="cb-17" name="rev" onclick="$(&#x27;#cbto-18&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="8e69a29547299d7eeeab4f86b21cb8db9801d753" /></td>
<td class="checkbox"><input id="cbto-17" name="rev_to" onclick="if ($(&#x27;#cb-17&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-16&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="8e69a29547299d7eeeab4f86b21cb8db9801d753" /></td>
<td class="committed_on">09/14/2013 04:45 am</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>fixed listing of Debian packages needed to be installed (closes <a href="../../../../issues/15.html" class="issue tracker-1 status-5 priority-3 priority-lowest closed" title="NPE in NewChatActivity (Closed)">#15</a>)</p></td>
</tr>
<tr class="changeset even">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/e5c4888f9372191817dcd10bf90efbacf8a7b66d" title="Revision e5c4888f">e5c4888f</a>
</td><td class="checkbox"><input id="cb-18" name="rev" onclick="$(&#x27;#cbto-19&#x27;).attr(&#x27;checked&#x27;,true);" type="radio" value="e5c4888f9372191817dcd10bf90efbacf8a7b66d" /></td>
<td class="checkbox"><input id="cbto-18" name="rev_to" onclick="if ($(&#x27;#cb-18&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-17&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="e5c4888f9372191817dcd10bf90efbacf8a7b66d" /></td>
<td class="committed_on">09/14/2013 04:43 am</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>update description of KeySync and how to use it</p></td>
</tr>
<tr class="changeset odd">
<td class="id">
  <a href="https://dev.guardianproject.info/projects/keysync/repository/revisions/6f6d181d2571a929e08b3ee0e02ab169440c6b71" title="Revision 6f6d181d">6f6d181d</a>
</td><td class="checkbox"></td>
<td class="checkbox"><input id="cbto-19" name="rev_to" onclick="if ($(&#x27;#cb-19&#x27;).attr(&#x27;checked&#x27;)) {$(&#x27;#cb-18&#x27;).attr(&#x27;checked&#x27;,true);}" type="radio" value="6f6d181d2571a929e08b3ee0e02ab169440c6b71" /></td>
<td class="committed_on">09/14/2013 04:43 am</td>
<td class="author">Hans-Christoph Steiner </td>
<td class="comments"><p>move README to markdown format</p></td>
</tr>
</tbody>
</table>
<input type="submit" value="View differences" />
</form>



        
        <div style="clear:both;"></div>
    </div>
</div>
</div>

<div id="ajax-indicator" style="display:none;"><span>Loading...</span></div>
<div id="ajax-modal" style="display:none;"></div>

<div id="footer">
  <div class="bgl"><div class="bgr">
    Powered by <a href="http://www.redmine.org/">Redmine</a> &copy; 2006-2013 Jean-Philippe Lang
  </div></div>
</div>
</div>
</div>

</body>
</html>
